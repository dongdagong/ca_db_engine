
/* client.c */

#include <stdio.h>
#include <pthread.h>
#include <pvm3.h>
#include "ca.h"
#include "bds.h"


// Client Communication Module

int ccm(char * request, RQ *rq) {

	int		myTid;
	int		maTid;
	int		info;
	static int	caTid = 0;
	static int	first = 1;
	FILE 		*fp;
	pthread_t	thread;
	int 		status;
	void 		*result;

 	myTid = pvm_mytid();

	if (first == 1) {
		// locate Master Agent

		fp = fopen(MA_TID_FILE,"r");
		fscanf(fp, "%d\n", &maTid);
		fclose(fp);

		// send request and my tid to Master Agent

		info = pvm_initsend(PvmDataDefault);
		info = pvm_packf("%s%d",request,myTid);

		info = pvm_send(maTid, 0);
		if (info != 0) {
			printf("ccm(): pvm_send error\n");
		}

		// receive the tid of the ca.c created for this client.

		pvm_recv(maTid, -1);
		pvm_upkint(&caTid, 1, 1);
	}

	printf("ccm(): clientTid = %x caTid = %x \n\n", myTid, caTid);

	mpRequest(&thread, request, rq, caTid);
	status = pthread_join(thread, &result);
	if (status != 0) {
		printf("ccm(): thread_join error\n");
		exit(1);
	}

	if (first == 1) first = 0;
}



int main() {
	char	*req = "select a from b where c = d";
	RQ		*rq;

	rq = rqInit(10000);
	if (rq == NULL) {
		printf("ccm(): cannot initialise rq \n");
		exit(0);
	}

	ccm(req, rq);
	printf("client.c main(): the final result is: \n\n");
	rqDisp(rq);
	pvm_exit();
}

