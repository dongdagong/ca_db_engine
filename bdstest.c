
// t e s t i n g . . .


#include <stdio.h>
#include "bds.h"
#include <stdlib.h>

// #include "util.h"


OL *buildOL(void) {

	int 	i;
	int 	j;
	OL		*ol;
	Object	*o;

	char *name, *bvalue;

	ol = olInit();

	j = (float)rand()/RAND_MAX * 7;		// [0, 7)

	for (i = 0; i < j; i++) {
		name = (char *)malloc(sizeof(char) * 16);
  		sprintf(name, "%d+name", i);

		bvalue = (char *)malloc(sizeof(char) * 16);
		sprintf(bvalue, "%d+bValue", i);

		o = i%3 == 0 ? objNewB((Type)'B', name, bvalue) : objNewO(name, i);

		olAdd(ol, o);
		objFree(o);

		free(name);
		free(bvalue);
	}
	return ol;
}


////////////////////////////   main   /////////////////////////////////////////

int main() {

	long i;
	long olMax = 5, esMax = 0, rsMax = 0, rqMax = 5;

	ObjId	objId;
	Type	type;
	char	*name, *bvalue;
	char	*str;

	Object	*r, *s, *t;
	Object 	*o, *b, *p, *objI;

	OLEntry	*ole;
	ESEntry *ese;
	ESEntry *nVSP = NULL;
	RSEntry	*rse;
	RQEntry *rqe;

	OL *ol;
	ES *es;
	RS *rs;
	RQ *rq;

	printf("\n*********************************************************** bds test\n\n");


	str = (char *)malloc(sizeof(char) * 16);

///////////////////////////////////////////////////////////////////////////////
/*
	printf("\n>>> test Object \n\n");

	//b = (Object *)malloc(sizeof(Object));


	b = objNewB('S', "iAmBasicObject", "myBvalue");
	p = objNewP("iAmPointerObject", NULL);
	objI = objNewO("iAMObjIdObject", 12345);

	o = objClone(b);

	strcpy(b->value.bvalue, "myBvalu-");

	objDisp(b);
	objDisp(p);
	objDisp(objI);

	objFree(b);
	objDisp(o);

	b = objNew("O:NULL:0");

	objDisp(b);
*/

///////////////////////////////////////////////////////////////////////////////

	printf("\n\n>>> test Object List \n\n");

	ol = olInit();
	olDisp(ol);

	printf("iterating the ol \n");

	olInitIterator(ol);
	while(olHasNext(ol)) {
		o = olNext(ol);
		objDisp(o);

	}
	printf("after olInit() olIsEmpty() returns %d \n", olIsEmpty(ol));

	printf("adding to ol \n");

	for (i = 0; i < olMax; i++) {
		name = (char *)malloc(sizeof(char) * 16);
  		sprintf(name, "name-%d", i);

		bvalue = (char *)malloc(sizeof(char) * 16);
		sprintf(bvalue, "bValue-%d", i);

		o = i%5 == 0 ? objNewB('D', name, bvalue) : objNewO(name, i);

		olAdd(ol, o);
		objFree(o);

		free(name);
		free(bvalue);
	}

	printf("after olAdd() olIsEmpty() returns %d \n", olIsEmpty(ol));

	olDisp(ol);

	printf("iterating the ol \n");

	olInitIterator(ol);
	while(olHasNext(ol)) {
		o = olNext(ol);
		objDisp(o);

	}

	printf("iterating the ol remove 3\n");

	olInitIterator(ol);
	while(olHasNext(ol)) {
		o = olNext(ol);
		if (o->type == 'D' )
 			olRemoveCur(ol);
		if (o->type == 'O' && (o->value.objId == 4 || o->value.objId == 3))
			olRemoveCur(ol);
	}

	olDisp(ol);

	printf("iterating the ol remove all\n");

	olInitIterator(ol);
	while(olHasNext(ol)) {
		o = olNext(ol);
 		olRemoveCur(ol);
	}


	printf("\nget & remove from ol \n");

	i = 0;
	while(!olIsEmpty(ol)) {
		o = olGet(ol);
		objDisp(o);
		olRemove(ol);
	}

	printf("iterating the ol last one\n");

	olInitIterator(ol);
	while(olHasNext(ol)) {
		o = olNext(ol);
		objDisp(o);

	}

	//free(o);		// since its already freed.
	printf("after olAdd() olIsEmpty() returns %d \n", olIsEmpty(ol));
	olFree(ol);
/*
///////////////////////////////////////////////////////////////////////////////

	printf("\n\n>>> test environment stack \n\n");


	es = esInit();
	if (es == NULL) {
		printf("es init error \n");
		exit(1);
	}
	esDisp(es);

	printf("After initialization, esIsEmpty() returns: %d \n", esIsEmpty(es));
	printf("push ->  es\n");

	for (i = 0; i < esMax; i++) {
		objId = i;
		type = i%5 == 0 ? 'b' : 'i';
		name = (char *)malloc(sizeof(char) * 16);
		sprintf(name, "name-%i", i);
		ol = buildOL();
		esPush(es, esNewEntry(objId, type, name, ol));
		free(name);
	}
	printf("After pushing, esIsEmpty() returns: %d \n", esIsEmpty(es));
	esDisp(es);

	printf("pop <- es\n");

	while (!esIsEmpty(es)) {
		eseDisp(esTop(es));
		olFree(esTop(es)->ol);		// not taken care of by esPop()
		esPop(es);
	}

	printf("After popping, esIsEmpty() returns: %d \n", esIsEmpty(es));

///////////////////////////////////////////////////////////////////////////////

	printf("\n\n>>> test result stack \n\n");

	rs = rsInit();
	rsDisp(rs);

	printf("After initialization, rsIsEmpty() returns: %d \n", 	rsIsEmpty(rs));

	printf("\npush -> rs \n");

	for (i = 0; i < rsMax; i++) {
		objId = i;
		type = i%5 == 0 ? 'b' : 'i';
		name = (char *)malloc(sizeof(char) * 16);
		sprintf(name, "name-%i", i);
		ol = buildOL();
		rse = rsNewEntry(objId, type, name, ol);
		rsPush(rs, rse);

		free(name);		// leak
		rseFree(rse);
	}

	printf("After pushing, rsIsEmpty() returns: %d \n", rsIsEmpty(rs));
	rsDisp(rs);
	//scanf("%s", str);
	printf("\npop <- rs\n");

	while (!rsIsEmpty(rs)) {
		rseDisp(rsTop(rs));
		rsPop(rs);
	}

	printf("After popping, rsIsEmpty() returns: %d \n", rsIsEmpty(rs));


///////////////////////////////////////////////////////////////////////////////

	printf("\n\n>>> test result queue \n\n");

	rq = rqInit(rqMax);
	rqDisp(rq);
	printf("After initialization, rqIsEmpty() returns: %d \n", 	rqIsEmpty(rq));

	printf("\nqueueing result queue \n");

	for (i = 0; i < rqMax; i++) {
		objId = i;
		type = i%5 == 0 ? 'b' : 'i';
		name = (char *)malloc(sizeof(char) * 16);
		sprintf(name, "name-%i", i);
		ol = buildOL();
		rqe = rqNewEntry(objId, type, name, ol);
		rqEnQ(rq, rqe);

		free(name);
		// olFree(ol);
		rqeFree(rqe);
	}

	printf("After queueing, rqIsEmpty() returns: %d \n", 	rqIsEmpty(rq));
	rqDisp(rq);

	printf("\ndequeueing result queue \n");

	while (!rqIsEmpty(rq)) {
		rqe = rqDeQ(rq);
		rqeDisp(rqe);
		rqeFree(rqe);
	}
	rqDisp(rq);

	printf("After dequeueing, rqIsEmpty() returns: %d \n", 	rqIsEmpty(rq));
*/
	scanf("%s", str);
	puts(" :)\n");
	return 0;
}
