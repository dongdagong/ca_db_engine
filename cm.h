
// This defines the structure of the argument for passing to mpRequestThread

typedef struct	mpRequestInfoTag {
	String	request;
	RQ		*rq;
	int		caTid;
} MpRequestInfo;

