
/* Communication Module */

#include <limits.h>
#include <pthread.h>
#include <pvm3.h>
#include "bds.h"
#include "cm.h"

void * mpRequestThread(void *requestInfoArg) {

	String	request;
	RQ		*rq;
	int		caTid;
	int		myTid;
	MpRequestInfo	*requestInfo;

	ObjId	rqeObjId, objId, i;
	Type	rqeType, type;
	String	rqeName, name;
	ObjId	olLen;
	String	bvalue;
	Object	*o;
	OL		*ol;
	RQEntry	*rqe;

	myTid = pvm_mytid();

	name = (char *)malloc(sizeof(char) * 1000);
	bvalue = (char *)malloc(sizeof(char) * 1000);

	requestInfo = (MpRequestInfo *)malloc(sizeof(MpRequestInfo));
	requestInfo = (MpRequestInfo *)requestInfoArg;
	request = requestInfo->request;
	rq = requestInfo->rq;
	caTid = requestInfo->caTid;

	pvm_initsend(PvmDataDefault);
	pvm_pkint(&myTid, 1, 1);
	pvm_pkstr(request);
	pvm_send(caTid, 0);

	while (1) {

		// recv rqe

		pvm_recv(-1, -1);
		pvm_upkulong(&rqeObjId, 1, 1);

		//printf("--- [%x <- %x] rqeObjId = %d %x\n", myTid, caTid, rqeObjId, rqeObjId);

		if (rqeObjId == -1) { //ULONG_MAX) {
			printf("mpRT(): RQ_END_SIGNAL received\n");
			rq->finish = 1;
			break;
		} else {
			pvm_upkbyte(&rqeType, 1, 1);
			pvm_upkstr(rqeName);
			pvm_upkulong(&olLen, 1, 1);

			ol = olInit();
			for (i = 0; i < olLen; i++) {
				pvm_upkbyte(&type, 1, 1);
				pvm_upkstr(name);
				switch (type) {
				case 'B': case 'S': case 'D': case 'I':
					pvm_upkstr(bvalue);
					o = objNewB(type, name, bvalue);
					break;
				case 'P':
					printf("mpRT(): should not receive P type objects\n");
					break;
				case 'O':
					pvm_upkulong(&objId, 1, 1);
					o = objNewO(name, objId);
					break;
				}
				olAdd(ol, o);
			}
			rqEnQ(rq, rqNewEntry(rqeObjId, rqeType, rqeName, ol));
			printf("- [%x <- %x] \n", myTid, caTid);
			rqeDisp(rqNewEntry(rqeObjId, rqeType, rqeName, ol));
		}
	}
	printf("mpRT(): finished\n\n");
}

// Assumes CA for this request is already spawned.

void mpRequest(pthread_t *thread, String request, RQ *rq, int caTid) {
	MpRequestInfo	*mpRequestInfo;

	mpRequestInfo = (MpRequestInfo *)malloc(sizeof(MpRequestInfo));
	mpRequestInfo->request = request;
	mpRequestInfo->rq = rq;
	mpRequestInfo->caTid = caTid;

	pthread_create(thread, NULL, mpRequestThread, (void *)mpRequestInfo);
}

