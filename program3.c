
/* Program 3 */


#include <stdio.h>
#include <pthread.h>
#include "bds.h"

MemEntry	*memDB[MAX_NUM_MEMENTRY];
RQ			*rq0, *rq1, *rq2;


RQEntry *findObject(RQ *rq, ObjId objId) {
	RQEntry	*temp;
	temp = rq->head;
	while (temp != NULL) {
		if (temp->objId == objId) {
			return temp;
		}
		temp = temp->next;
	}
	return NULL;
}

RSEntry *findObjectRS(RS *rs, ObjId objId) {
	RSEntry	*temp;
	temp = rs->top;
	while (temp != NULL) {
		if (temp->objId == objId) {
			return temp;
		}
		temp = temp->next;
	}
	return NULL;
}

void initRS(RS *rs, RQ *rq, OL *arg, char *str) {

	Object	*o;

	ESEntry *ese;
	RQEntry *rqe;

	// a) populate arg

	o = objNew(str);
	olAdd(arg, o);

	// b)

	while (!olIsEmpty(arg)) {
		o = olGet(arg);
		ese = unnest(memDB, o);
		rqEnQ(rq, ese2rqe(ese));
		olRemove(arg);
	}

	// c)

	rq2rs(rq, rs);
}


void *cal1(Object *o) {
	Object	*r, *s, *t, *u, *a, *a1;

	ESEntry	*ese, *ese2, *ese3, *ese4, *ese5;
	RSEntry	*rse;
	RQEntry	*rqe;

	ES	*es;
	RS	*rs;
	OL	*arg;

	int	flag = 0;

	a1 = objNew("O:enrolment:NOTNULL");
	a = objNewP("enrolHistory", a1);

	es = esInit();
	rs = rsInit();
	arg = olInit();

	initRS(rs, rq1, arg, "O:StudentC:3");
	//disp(es, rs, rq1, arg, "cal 1");

	// selectUnnest

	rse = rsTop(rs);
	olInitIterator(rse->ol);
	while (olHasNext(rse->ol)) {
		r = olNext(rse->ol);
		esPush(es, unnest(memDB, r));
		//objDisp(r);
		//eseDisp(unnest(memDB, r));
		flag = 0;
		ese = esTop(es);
		olInitIterator(ese->ol);
		while (olHasNext(ese->ol)) {
			s = olNext(ese->ol);
			//objDisp(s);
			if (strcmp(s->name, a->name) == 0) {
				esPush(es, unnest(memDB, s));
				//eseDisp(unnest(memDB, s));

				ese2 = esTop(es);
				olInitIterator(ese2->ol);
				while (olHasNext(ese2->ol)) {
					t = olNext(ese2->ol);
					//objDisp(s);
					if (strcmp(t->name, a1->name) == 0) {
						rqEnQ(rq1, ese2rqe(unnest(memDB, t)));
						flag = 1;
					}
				}
				if (flag == 1) {
					rqEnQ(rq1, ese2rqe(esTop(es)));
				}
				esPop(es);
			}
		}


		if (flag == 1) {
			ese = esTop(es);
			olInitIterator(ese->ol);
			while (olHasNext(ese->ol)) {
				s = olNext(ese->ol);
				if (strcmp(s->name, "IsA") == 0) {
					esPush(es, unnest(memDB, s));

    				// unnest

					ese3 = esTop(es);
					olInitIterator(ese3->ol);
					while (olHasNext(ese3->ol)) {
						t = olNext(ese3->ol);
						//objDisp(t);
						if (t->type == 'O' && t->value.bvalue != NULL) {

							esPush(es, unnest(memDB, t));

							ese4 = esTop(es);
							olInitIterator(ese4->ol);
							while (olHasNext(ese4->ol)) {
								u = olNext(ese4->ol);
								if (u->type == 'O' && u->value.bvalue != NULL) {
									rqEnQ(rq1, ese2rqe(unnest(memDB, u)));
								}
							}
							rqEnQ(rq1, ese2rqe(esTop(es)));
							esPop(es);
						}
					}
					rqEnQ(rq1, ese2rqe(esTop(es)));
					esPop(es);
				}
			}
			ese5 = esTop(es);
			ese5->name = (char *)strdup(o->name);
			rqEnQ(rq1, ese2rqe(ese5));
		}
		esPop(es);
	}

	disp(es, rs, rq1, arg, "cal 1 completed"); pause();
	//pause();
	//wait for termination signal;
	//pthread_exit(NULL);

}


void unnestR(RQ *rq, Object *obj) {
	ESEntry	*ese;
	Object	*o;

	ese = unnest(memDB, obj);
	//eseDisp(ese);

	rqEnQ(rq, ese2rqe(ese));
	olInitIterator(ese->ol);
	while (olHasNext(ese->ol)) {
		o = olNext(ese->ol);
		if (o->type == 'O' && strcmp(o->name, "IsA") != 0 && o->value.bvalue != 0) {
			unnestR(rq, o);
		}
	}
}


void unnestR2(RQ *rq, Object *obj) {
	ESEntry	*ese;
	Object	*o;

	//objDisp(obj);
	//pause();
	ese = unnest(memDB, obj);
	rqEnQ(rq, ese2rqe(ese));
	olInitIterator(ese->ol);
	while (olHasNext(ese->ol)) {
		o = olNext(ese->ol);
		if (o->type == 'O') { // && strcmp(o->name, "IsA") != 0) {
			unnestR2(rq, o);
		}
	}
}


ESEntry *unnest3(RQ *rq, Object *o) {

	ESEntry	*ese;
	RQEntry	*rqe;

	switch (o->type) {
	case 'O':	// ObjId
		rqe = (RQEntry *)findObject(rq2, (ObjId)(o->value.bvalue));
		if (rqe == NULL)
			ese = NULL;
		else
			ese = rqe2ese(rqe);
		break;
	case 'S':
		break;
	case 'D':
		return NULL;
	default:
		printf("unnest(): should not reach this point.\n");
	}
	return ese;
}


void unnestR3(RQ *rq, Object *obj) {
	ESEntry	*ese;
	Object	*o;


	ese = unnest3(rq, obj);
	//eseDisp(ese);
	if (ese != NULL) {
	rqEnQ(rq, ese2rqe(ese));
	olInitIterator(ese->ol);
	while (olHasNext(ese->ol)) {
		o = olNext(ese->ol);
		if (o->type == 'O' && strcmp(o->name, "IsA") != 0 && o->value.bvalue != 0) {
			unnestR3(rq, o);
		}
	}
	}
}

void *cal2(Object *o) {

	Object	*r, *s, *t, *u, *v, *w, *a, *a1;

	ESEntry	*ese, *ese2, *ese3, *ese4;
	RSEntry	*rse;
	RQEntry	*rqe;

	ES	*es;
	RS	*rs;
	OL	*arg;

	int	flag = 0;

	o = objNew("O:PaperOfferingC:7");

	es = esInit();
	rs = rsInit();
	arg = olInit();

	initRS(rs, rq2, arg, "O:PaperOfferingC:7");

	//disp(es, rs, rq2, arg, "cal 2");

	// selectWhere()

	// a)
	a1 = objNew("I:year:2003");
	a = objNewP("semester", a1);

	// b)
	rse = rsTop(rs);
	olInitIterator(rse->ol);
	while (olHasNext(rse->ol)) {
		r = olNext(rse->ol);

		esPush(es, unnest(memDB, r));
		flag = 0;
		ese = esTop(es);
		olInitIterator(ese->ol);
		while (olHasNext(ese->ol)) {
			s = olNext(ese->ol);
			if (strcmp(s->name, a->name) == 0) {
				esPush(es, unnest(memDB, s));
				ese2 = esTop(es);
				olInitIterator(ese2->ol);
				while (olHasNext(ese2->ol)) {
					t = olNext(ese2->ol);
					if (strcmp(t->name, a1->name) == 0 && t->type == a1->type && strcmp(t->value.bvalue, a1->value.bvalue) == 0) {
						flag = 1;
						//rqEnQ(rq2, ese2rqe(esTop(es)));
						break;
					}
				}
				esPop(es);
			}
		}
		if (flag == 1) {
			ese3 = esTop(es);
			ese3->name = (char *)strdup(o->name);
			rqEnQ(rq2, ese2rqe(esTop(es)));
			olInitIterator(ese3->ol);
			while (olHasNext(ese3->ol)) {
				u = olNext(ese3->ol);
				unnestR(rq2, u);
			}
		}
		esPop(es);
	}

	rsFree(rs);

	disp(es, rs, rq2, arg, "cal 2 completed"); pause();
/*
	selectWhere(es, rs, rq2,  * );
	wait for termination signal;
	pthread_exit(NULL);
*/
}


void matchClassName(RS *rs, OL *arg, String str) {

	Object *r, *s, *o;
	RSEntry *rse;
	ESEntry *ese;

	// a) populate arg

	o = objNew(str);
	olAdd(arg, o);

	// b)

	olInitIterator(arg);
	while (olHasNext(arg)) {
		r = olNext(arg);
		rse = rsTop(rs);
		olInitIterator(rse->ol);
		while(olHasNext(rse->ol)) {
			s = olNext(rse->ol);
			if (strcmp(r->name, s->name) == 0) {
				if (strcmp(o->name, "StudentC") == 0)
					cal1(s);
				else
					cal2(s);
				break;
			}
		}
	}

	// c)

	olFree(arg);
}


// Pushes all sub-objects of rqe into this result queue.

void pushResult(RQ *rq, RQEntry *rqe) {
	Object	*o;
	olInitIterator(rqe->ol);
	while (olHasNext(rqe->ol)) {
		o = olNext(rqe->ol);
		unnestR3(rq, o);
	}
}


Boolean nameInArg(OL *arg, char *name) {
	olInitIterator(arg);
	while (olHasNext(arg)) {
		if (strcmp(olNext(arg)->name, name) == 0)
			return TRUE;
	}
	return FALSE;
}


// Removes the first entry that matches objId

Boolean rsRmEntry(RS *rs, ObjId objId) {

	RSEntry	*rse, *prev, *temp;

	rse = rs->top;
	prev = NULL;

	while (rse != NULL) {

		if (rse->objId == objId) {	// found, remove it.

			if (prev == NULL) {		// removing the first entry
				rs->top = rse->next;
			} else {
				prev->next = rse->next;
			}
			rs->depth--;
		}
		prev = rse;
		rse = rse->next;
	}
}

// Removes this object from all entries in the RS where the objId match.

void rsRmObjs(RS *rs, ObjId objId, char *name) {
	RSEntry	*rse;
	OLEntry	*p, *n;
	Object	*r;

	name = (char *)strdup(name);

	rse = rs->top;
	while (rse != NULL) {
		if (rse->objId == objId) {
			p = rse->ol->p;
			n = rse->ol->n;

			//printf("  rse %u  \n", objId);
			olInitIterator(rse->ol);
			while (olHasNext(rse->ol)) {
				r = olNext(rse->ol);
				//printf(" 1 x %s ", name); olDisp(rse->ol);
				if (strcmp(r->name, name) == 0)
					olRemoveCur(rse->ol);
				//printf(" 2 "); olDisp(rse->ol);
			}

			rse->ol->p = p;
			rse->ol->n = n;
		}
		rse = rse->next;
	}
}


void projectName2(RS *rs, RSEntry *rse, OL *arg) {
	Object	*s, *o, *t;
	OL		*newArg;

	//printf(": %u \n arg: ", rse->objId); olDisp(arg);
	olInitIterator(rse->ol);
	while (olHasNext(rse->ol)) {
		s = olNext(rse->ol);
		if (arg->len > 0 && !nameInArg(arg, s->name)) {
   			t = objClone(s);

			//printf("ddd  %u  %s\n", rse->objId, s->name);
			rsRmObjs(rs, rse->objId, s->name);
			// and remove this current object's related RSEntry as well.
			//printf("t >> "); objDisp(t);
			if (t->type == 'O' && t->value.objId != 0) {
				rsRmEntry(rs, t->value.objId);
			}
		} else {
			if (s->type == 'O') {
				// build next level arg OL
				newArg = olInit();
				olInitIterator(arg);
				while (olHasNext(arg)) {
					o = olNext(arg);
					if (s->name != NULL && strcmp(o->name, s->name) == 0) {
						//if (o != NULL)
						o = o->value.ptr;
						if (o != NULL)
							olAdd(newArg, o);
					}
				}
				//printf("arg "); olDisp(arg);
				//printf("new "); olDisp(newArg);
				projectName2(rs, findObjectRS(rs, s->value.objId), newArg);
				//printf("s "); objDisp(s);
				//printf("rse "); rseDisp(findObjectRS(rs, s->value.objId));
			}
		}
	}
}


void projectNames(ES *es, RS *rs, RQ *rq, OL *arg) {
	Object	*a,
			*a11, *a12, *a13,
			*a21, *a22, *a23, *a24,
			*a31, *a32, *a33, *a34,
			*a41, *a42, *a43,
			*a51, *a52, *a53, *a54, *a55;

	RSEntry	*rse;

	a55	= objNew("S:cpName:NULL");
	a54	= objNewP("campus", a55);
	a53	= objNewP("offeredAt", a54);
	a52	= objNewP("enrolment", a53);
	a51	= objNewP("enrolHistory", a52);

	a43	= objNew("O:semester:NULL");
	a42	= objNewP("enrolment", a43);
	a41	= objNewP("enrolHistory", a42);

	a34	= objNew("S:title:NULL");
	a33	= objNewP("IsA", a34);
	a32	= objNewP("enrolment", a33);
	a31	= objNewP("enrolHistory", a32);

	a24	= objNew("S:number:NULL");
	a23	= objNewP("IsA", a24);
	a22	= objNewP("enrolment", a23);
	a21	= objNewP("enrolHistory", a22);

	a13	= objNew("S:lastName:NULL");
	a12	= objNewP("Name", a13);
	a11	= objNewP("IsA", a12);

	a	= objNewP("StudentC", NULL);


	olAdd(arg, a11); olAdd(arg, a21); olAdd(arg, a31); olAdd(arg, a41); olAdd(arg, a51);
	//olDisp(arg); exit(0);

	rse = rs->top;
	while (rse != NULL) {

		if (rse->name != NULL && strcmp(rse->name, a->name) == 0) {
			//rseDisp(rse);
			//printf("projectnames new round   "); olDisp(arg);
			projectName2(rs, rse, arg);
		}
		rse = rse->next;
	}
}

void join(ES *es, RS *rs, OL *arg) {

	Object	*r, *s, *u, *w, *y, *z;
	Object	*a, *a1, *a2, *a3, *a4;
	ESEntry	*ese, *ese2, *ese3, *t, *v;
	RSEntry	*rse;
	RQEntry	*rqe, *x;
	int 	flag = 0;

	// a)
	a3 = objNew("O:paper:NULL");
	a2 = objNewP("enrolment", a3);
	a1 = objNewP("enrolHistory", a2);
	a  = objNewP("StudentC", a1);

	rqe = rq1->head;
	while (rqe != NULL) {
		if (rqe->name != NULL && strcmp(rqe->name, a->name) == 0) {
			//printf("%u %s \n", rqe->objId, a->name);
			olInitIterator(rqe->ol);
			while (olHasNext(rqe->ol)) {
				s = olNext(rqe->ol);
				//objDisp(s);
				if (strcmp(a1->name, s->name) == 0) {	// enrolHistory
					t = unnest(memDB, s);
					//eseDisp(t);
					olInitIterator(t->ol);
					while (olHasNext(t->ol)) {
						u = olNext(t->ol);
						//objDisp(u);
						v = unnest(memDB, u);
						//eseDisp(v);
						olInitIterator(v->ol);
						while (olHasNext(v->ol)) {
							w = olNext(v->ol);
							if (strcmp(a3->name, w->name) == 0) {
								//objDisp(w);
								x = findObject(rq2, w->value.objId);
								//if (x == NULL) printf("x is NULL\n");	else rqeDisp(x);
								if (x != NULL) {
									flag = 1;
									//rqeDisp(x);
									//eseDisp(v);
									pushResult(rq0, x);
									//printf("%u  \n", x->objId);
									// x UNION v
									olInitIterator(v->ol);
									while (olHasNext(v->ol)) {
										y = olNext(v->ol);
										if (strcmp(y->name, "paper") == 0)
											olRemoveCur(v->ol);
									}
									//olDisp(v->ol);
									olInitIterator(x->ol);
									while (olHasNext(x->ol)) {
										olAdd(v->ol, olNext(x->ol));
									}
									//olDisp(v->ol);
									// push x UNION v
									rqEnQ(rq0, ese2rqe(v));
								} else {
									olRemoveCur(t->ol);
									//olDisp(t->ol);

								}
							}
						}
					}
				}
				if (flag == 1) {
					rqEnQ(rq0,ese2rqe(t));
				}
			}
		}
		if (flag == 1) {
			rqEnQ(rq0, rqe);
			//rqeDisp(rqe);
			//objDisp(olGet(rqe->ol));
			unnestR(rq0, olGet(rqe->ol));
			flag = 0;
		}
		rqe = rqe->next;
	}


	// rqDisp(rq1); rqDisp(rq2); rqDisp(rq0);
	rq2rs(rq0, rs);
	disp(es, rs, rq0, arg, "join completed"); pause();
}



void *tsam(void *requestInfo) {

	ES		*es;
	RS		*rs;
	OL		*arg;
	String	request;

	pthread_t	thread1, thread2;
	int			status;

	es = esInit();
	rs = rsInit();
	arg = olInit();

	request = (char *)malloc(sizeof(char) * 100);
	strcpy(request, (char *)requestInfo);
	printf("tsam(): %s\n", request);

	initRS(rs, rq0, arg, "O:NULL:0");
	if (strcmp(request, "joinproject") == 0) {


		ptRequest(&thread1, "cal1");
		ptRequest(&thread2, "cal2");

		// wait until both rq1 and rq2 are finished
		pthread_join(thread1, (void **)&status);
		pthread_join(thread2, (void **)&status);

		rsFree(rs);
		join(es, rs, arg);
		projectNames(es, rs, rq0, arg);
		disp(es, rs, rq0, arg, "projection completed");
	}

	if (strcmp(request, "cal1") == 0) {
		matchClassName(rs, arg, "S:StudentC:NULL");
	}

	if (strcmp(request, "cal2") == 0) {
		matchClassName(rs, arg, "S:PaperOfferingC:NULL");
	}
}


int ptRequest(pthread_t *thread, String request) {
	pthread_create(thread, NULL, tsam, (void *)request);
	return;
}


void mpRequest(String request) {

}


int main(void) {
	pthread_t	thread;
	int			status;

	rq0 = rqInit(1000);
	rq1 = rqInit(1000);
	rq2 = rqInit(1000);

	dbRead(memDB);
	ptRequest(&thread, "joinproject");
	pthread_join(thread, (void **)&status);
}
