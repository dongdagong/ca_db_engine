/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#ifndef _MA_H_RPCGEN
#define _MA_H_RPCGEN

#include <rpc/rpc.h>


#ifdef __cplusplus
extern "C" {
#endif


struct MCARPCListener {
	char *host;
	u_long progNum;
	u_long verNum;
	int portNum;
};
typedef struct MCARPCListener MCARPCListener;

#define MASTERAGENT 0x200000001
#define VERSION1 1

#if defined(__STDC__) || defined(__cplusplus)
#define rpc_ma 1
extern  MCARPCListener * rpc_ma_1(char **, CLIENT *);
extern  MCARPCListener * rpc_ma_1_svc(char **, struct svc_req *);
extern int masteragent_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define rpc_ma 1
extern  MCARPCListener * rpc_ma_1();
extern  MCARPCListener * rpc_ma_1_svc();
extern int masteragent_1_freeresult ();
#endif /* K&R C */

/* the xdr functions */

#if defined(__STDC__) || defined(__cplusplus)
extern  bool_t xdr_MCARPCListener (XDR *, MCARPCListener*);

#else /* K&R C */
extern bool_t xdr_MCARPCListener ();

#endif /* K&R C */

#ifdef __cplusplus
}
#endif

#endif /* !_MA_H_RPCGEN */
