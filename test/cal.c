
/*
 * CA on Local layer
 *
 * test comm mechanisms.
 */


// return 1) the number of primes 2) the primes between the given range.


#include <stdio.h>
#include <pvm3.h>

main() {

	unsigned long range[2];
	unsigned long num;
	unsigned long i, j;
	int numBufId;
	int resultBufId;
	int p;

	int ptid;
	int cc;

	//FILE *fp;

	//fp = fopen("/home/dagong/ca/err", "a");	// redirect error messages
	//pvm_catchout(fp);

	ptid = pvm_parent();

	cc = pvm_recv(ptid, -1);
	//pvm_bufinfo(cc, (int*)0, (int*)0, &tid);
	pvm_upkulong(range, 2, 1);
	// pk upk pk upk pk upk

	//numBufId = pvm_initsend(PvmDataDefault);
	numBufId = pvm_mkbuf(PvmDataDefault);
	resultBufId = pvm_mkbuf(PvmDataDefault);

	pvm_setsbuf(resultBufId); 

	num = 0;
	for (i = range[0]; i <= range[1]; i++) {
		p = 1;
		for (j = 2; j * j <= i; j++) {
			if (i % j == 0) {
				p = 0;
				break;
			}
		}

		if (p && i > 2) {
			num++;
			//pvm_pkulong(&i, 1, 1);
		}
	}

	//fprintf(fp, "if something worng the err msg goes here\n");

	pvm_setsbuf(numBufId);
	pvm_pkulong(&num, 1, 1);
	pvm_send(ptid, 1);

	//pvm_setsbuf(resultBufId);
	//pvm_send(ptid, 1);

	pvm_freebuf(numBufId);
	pvm_freebuf(resultBufId);

	//fclose(fp);
	pvm_exit();
	exit(0);
}
