

/*

can i share memory space between pvm tasks running on the same node?

don't forget to test it on SPARC.

*/


#include <stdio.h>
#include <pvm3.h>

#define MAX 1000000

int *sharedMem;



int main() {
	int mytid, tid;
	int cc;

 sharedMem = (int *)malloc(sizeof(int) * 1);

	*sharedMem = 123;


	printf("main(): %x\n", pvm_mytid());
	printf("main(); %p\n", sharedMem);

	printf("spawning innodeMemShareT \n");
	cc = pvm_spawn("/home/dagong/ca/innodeMemShareT", (char**)0, 1, ".", 1, &tid);

	if (cc == 1) {
		pvm_initsend(PvmDataDefault);
		pvm_packf("%p", sharedMem);
		pvm_send(tid, 1);
	} else
		printf("can't start child task\n");

}

