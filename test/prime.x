
struct Range {
	unsigned long a;
	unsigned long b;
};

program master {
	version masterver {
		unsigned long prime(struct Range) = 1;
	} = 1;
} = 0x200000001;
