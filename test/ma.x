

struct MCARPCListener {
	string 			host<8>;
	unsigned long	progNum;
	unsigned long 	verNum;
	int				portNum;
};

program MASTERAGENT {
	version VERSION1 {
		MCARPCListener rpc_ma(string request) = 1;
	} = 1;
} = 0x200000001;
