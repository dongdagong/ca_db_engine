struct Element{
	int i;
};

struct Queue{
	Element * e;
	Element * next;
};

program MASTER {
	version MASTERVER {
		Queue prime(char * s) = 1;
	} = 1;
} = 0x200000001;
