/*
 * File: main.c
 * Testing facility.
 */


#include <stdio.h>
#include "stack.h"
#include "common.h"


// belongs to common.c
/*
 * Return the duplicate of the given stirng.
 */

char * strDup(char * s) {

	char *p;

	p = (char *)malloc(strlen(s) + 1);
	return (p != NULL ? strcpy(p, s) : NULL);
}


// t e s t i n g . . .

int main() {

	long i;
	long MAX = 1000000;
	char cP[20];

	StackElement *sea[MAX];
	EnvEntry *nVSP = NULL;
	Env *env;

	StackElement se;
	Res *res;


	printf("\n\n-- test environment stack -- \n");

	env = initEnv();

	printf("After initialization, emptyEnv() returns: %d \n", emptyEnv(env));
	printf("push ->  env\n");

	for (i = 0; i < MAX; i++) {
		sea[i] = (StackElement *)malloc(sizeof(StackElement));
		if (!sea[i])
			printf("no space for sea[%u]\n", i);

		if (i%5 == 0) {
			sea[i]->type = 'b';
			sprintf(cP, "%d", i);
			sea[i]->value.bvalue = strDup(cP);
			pushEnv(env, i, sea[i], nVSP);
			printf("%s\n", sea[i]->value.bvalue);
		} else {
			sea[i]->type = 'i';
			sea[i]->value.objId = i;
			pushEnv(env, i, sea[i], nVSP);
			printf("%d\n",sea[i]->value.objId);
		}
	}

	printf("pop <- env\n");

	while (!emptyEnv(env)) {

		EnvEntry o;

		o = topEnv(env);
		if (o.objData->type == 'b') {
			printf("b %s\n", o.objData->value.bvalue);
		} else if (o.objData->type == 'i') {
			printf("i %d\n", o.objData->value.objId);
		}

		popEnv(env);
		// free up memory not taken care of by popEnv().
	}

	sea[0]->type = 'i';
	sea[0]->value.objId = 0;
	sea[1]->type = 'i';
	sea[1]->value.objId = 1;

	printf("emptyEnv() returns: %d \n", 	emptyEnv(env));
	printf("pushing sea[0] and sea[1]\n");
	pushEnv(env, 0, sea[0], nVSP);
	pushEnv(env, 1, sea[1], nVSP);
	printf("emptyEnv() returns: %d \n", 	emptyEnv(env));

	printf("topping: %d \n", topEnv(env).objData->value.objId);
	popEnv(env);
	printf("topping: %d \n", topEnv(env).objData->value.objId);
	popEnv(env);

	printf("emptyEnv() returns: %d \n", 	emptyEnv(env));



	printf("\n\n-- test result stack -- \n");

	res = initRes();

	printf("After initialization, emptyRes() returns: %d \n", 	emptyRes(res));

	printf("pushing 1 2 3\n");
	se.type = 'b';
	se.value.bvalue = strDup("result 1");
	pushRes(res, 1, &se);
	se.value.bvalue = strDup("result 2");
	pushRes(res, 2, &se);
	se.value.bvalue = strDup("result 3");
	pushRes(res, 3, &se);

	printf("popping: \n%s\n", topRes(res).objData->value.bvalue);
	popRes(res);
	printf("%s\n", topRes(res).objData->value.bvalue);
	popRes(res);

	printf("after pop 3 and 2 emptyRes() returns: %d \n", emptyRes(res));

	printf("pushing 4\n");
	se.value.bvalue = strDup("result 4");
	pushRes(res, 4, &se);

	printf("popping:\n%s\n", topRes(res).objData->value.bvalue);
	popRes(res);
	printf("%s\n", topRes(res).objData->value.bvalue);
	popRes(res);

	printf("after pop 4 and 1 emptyRes() returns: %d \n", emptyRes(res));


	printf("bulk pushing res \n");

	for (i = 0; i < MAX; i++) {

		if (i%7 ==  0) {
			se.type = 'b';
			sprintf(cP, "%d", i);
			se.value.bvalue = strDup(cP);
			pushRes(res, i, &se);
			printf("%s\n", se.value.bvalue);
			free((void *)(se.value.bvalue));
		} else {
			se.type = 'i';
			se.value.objId = i;
			pushRes(res, i, &se);
			printf("%d\n",se.value.objId);
		}
	}

	printf("bulk popping res\n");

	while (!emptyRes(res)) {

		ResEntry o;

		o = topRes(res);
		if (o.objData->type == 'b') {
			printf("s %s\n", o.objData->value.bvalue);
		} else if (o.objData->type == 'i') {
			printf("i %d\n", o.objData->value.objId);
		}

		popRes(res);
	}

	scanf("%s", cP);
	puts(". ");

	return 0;
}
