/*
 * CA on Distributed layer.
 *
 * test comm mechanisms.
 * Use prime number finder as an example.
 */


#include <stdio.h>
#include <pvm3.h>
#include "prime.h"
#include <pthread.h>

#define deb ug

int cc = 0;
unsigned long *len3; // for return value from thread that monitors RPC call.
pthread_mutex_t m;


void *threadFunc(void *v) {

	CLIENT *clnt;

	Range * r;
	Range range;

	r = (Range *)v;

	range.a = r->a;
	range.b = r->b;

	// range is ready


	printf("RPCalling 7501\n");
	clnt = (CLIENT *)clnt_create ("it007501", master, masterver, "TCP");
	if (clnt == NULL) {
		clnt_pcreateerror ("it007501");
		exit (1);
	}

	len3 = (unsigned long *)malloc(sizeof(unsigned long));
	len3 = prime_1(&range, clnt);
	if (len3 == (u_long *) NULL) {
		printf("call failed\n");
		// clnt_perror (clnt, "call failed");
	} else
		printf("len3 is %u\n", *len3); // printf thread safe.

	clnt_destroy (clnt);


	pthread_mutex_lock (&m);
	cc++;
	pthread_mutex_unlock (&m);


	pthread_exit(NULL);
}



int main(void) {

	unsigned long a = 0;
	unsigned long d = 1000000;

	/*  				serial approach		parallel 3
		10,000,000		103					47 RPC time out  
		1,000,000		4 					2.6
		5,000,000		37 					16

	*/

	unsigned long b, c;

	unsigned long *result;
	unsigned long *result2;

	unsigned long i;
	int tid1, tid2, ptid;

	unsigned long buf[2];
	unsigned long len;

	struct Range range;



	pthread_t thread;



	// launch listener


	// receive task
	// a, b preassigned for the moment.


	// create sub-task

	b = a + (d - a) / 3;
	c = a + (d - a) / 3 * 2;

	// [a, b) for N1 PVM1, MP
	// [b, c) for N2 PVM1, MP
	// [c, d] for N3 PVM2, RPC

	printf("\na %u  b %u  c %u  d %u \n\n", a, b, c, d);


	// distribute sub-task

	// create cal13 (on N1) and send task [a, b) to it

	buf[0] = a;
	buf[1] = b;
	ptid = pvm_parent();

	printf("cad.c main(): i'm t%x\n", pvm_mytid());

	printf("spawning cal 13 on 7599 ...\n");
	cc = pvm_spawn("/home/dagong/ca/cal", (char**)0, 1, "it017599", 1, &tid1);

	if (cc == 1) {
		pvm_initsend(PvmDataDefault);
		pvm_pkulong(buf, 2, 1);
		pvm_send(tid1, 1);
	} else
		printf("can't start cal \n");



	// create call12 (on N2) and send task [b, c) to it
	printf("spawning cal 12 on 1930 ...\n");
	cc = pvm_spawn("/home/dagong/ca/cal", (char**)0, 1, "it001930", 1, &tid2);

	if (cc == 1) {
		pvm_initsend(PvmDataDefault);
		pvm_pkulong(&b, 1, 1);
		pvm_pkulong(&c, 1, 1);
		pvm_send(tid2, 1);
	} else
		printf("can't start cal \n");


	// send task [c, d] to cad21 on N3 PVM2.
	// Async receiving in PThread for sync RPC call.


	range.a = c;
	range.b = d;


	printf("creating thread for RPC call \n");

	pthread_mutex_init(&m, NULL);
	pthread_create( &thread, NULL, threadFunc, (void *)&range);
	pthread_mutex_destroy(&m);




	// non-blocking receiving
	printf("non-blocking receiving ..\n");

	cc = 0;
	while (cc < 3) {
		if (pvm_nrecv(tid1, -1) != 0) {
			pvm_upkulong(&len, 1, 1);
			printf("len1: %u\n", len);
			pthread_mutex_lock (&m);
			cc++;
			pthread_mutex_unlock (&m);
		}

		if (pvm_nrecv(tid2, -1) != 0) {
			pvm_upkulong(&len, 1, 1);
			printf("len2: %u\n", len);
			pthread_mutex_lock (&m);
			cc++;
			pthread_mutex_unlock (&m);
		}

		/*if (len3 != 0) {	// this value is updated by the thread spawned above.
			printf("len3: %u\n", len3);
			cc ++;
		}*/
	}






	// receive results from cal13

/*	printf("receiving results from cal 13 ..\n");

	cc = pvm_recv(tid1, -1);
	//pvm_bufinfo(cc, (int*)0, (int*)0, &tid);
	pvm_upkulong(&len, 1, 1);
	printf("len: %u\n", len);*/

/*	pvm_recv(tid1, -1);
	printf("unpacking results ..\n");
	result = (unsigned long *)malloc(sizeof(unsigned long) * len);
	pvm_upkulong(result, len, 1);*/


	#ifdef debug
	for (i = 0; i < len; i++) {
		printf("%u: %u \n", i, result[i]);

	}
	#endif


	// receive results from cal12

/*	printf("receiving results from cal 12 ..\n");

	cc = pvm_recv(tid2, -1);
	//pvm_bufinfo(cc, (int*)0, (int*)0, &tid);
	pvm_upkulong(&len, 1, 1);
	printf("len: %u\n", len);*/

/*	pvm_recv(tid2, -1);
	printf("unpacking results ..\n");
	result2 = (unsigned long *)malloc(sizeof(unsigned long) * len);
	if (result2 == NULL)
		printf("malloc err\n");
	pvm_upkulong(result2, len, 1);*/


	#ifdef debug
	for (i = 0; i < len; i++) {
		printf("%u: %u \n", i, result2[i]);

	}
	#endif




	printf(".\n");

   pthread_exit(NULL);

	return 0;



}

