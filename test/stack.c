
/*
 * File: stack.c
 * Operations for Environment Stack and Result Stack.
 */


#include <stdio.h> 
#include "stack.h" 


/*
 * Initialize a environment stack.
 */

Env *initEnv(void) {
	Env *s;
	s = (Env *)malloc(sizeof(Env));
	if (s == NULL) {
		printf("initEnv(): Can not initialize environment stack.\n");
		return NULL;
	}
	s->top = NULL;
	s->depth = 0;
	
	return s;
}


/*
 * Initialize a Result stack.
 */

Res *initRes(void) {
	Res *s;
	s = (Res *)malloc(sizeof(Res));
	if (s == NULL) {
		printf("initEnv(): Can not initialize result stack.\n");
		return NULL;
	}
	s->top = NULL;
	//s->depth = 0;

	return s;
}

/*
 * Return TRUE if the stack is empty, FALSE otherwise.
 * Only should be called after initEnv.
 */

Boolean emptyEnv(Env *s) {
	
	return (s->top == NULL); 
	//this works only on SPARC machine before changing to use *env instead of env in main(), why?
}


/*
 * Return TRUE if the stack is empty, FALSE otherwise.
 * Only should be called after initEnv.
 */

Boolean emptyRes(Res *s) {
	
	return (s->top == NULL); 
}



/*
 * Adds a new element to the top of environment stack, returns TRUE if success, False otherwise.
 */

Boolean pushEnv(Env *env, ObjId objId, StackElement *objData, EnvEntry *nextVisibleScope) {
	
	// create a new stack entry
	
	EnvEntry *newEntry;
	
	newEntry = (EnvEntry *)malloc(sizeof(EnvEntry));
	if (newEntry == NULL) {
		printf("pushEnv(): can not allocate memory for newEntry.\n");
		return FALSE;
	}
/* comment out since environment stack only stores the pointers
 
	newEntry->objData = (StackElement *)malloc(sizeof(StackElement));
	if (newEntry->objData == NULL) {
		printf("pushEnv(): can not allocate memory for newEntry->objData.\n");
		return FALSE;
	}
	
	newEntry->objId = objId;
	newEntry->nextVisibleScope = nextVisibleScope;
	
	newEntry->objData->type = objData->type;   
	
	switch(newEntry->objData->type) {
	
	case 'b':	// basic
		newEntry->objData->value.bvalue = (String)malloc(sizeof(objData->value.bvalue) + 1);
		if (newEntry->objData->value.bvalue == NULL) {
			printf("pushEnv(): can not allocate memory for newEntry->objData->value.bvalue\n");
			return FALSE;
		}
		
		strcpy(newEntry->objData->value.bvalue, objData->value.bvalue);
		break;
	
	case 'p':	// pointer
		newEntry->objData->value.ptr = objData->value.ptr;
		break;
	
	case 'i':	// ObjId
		newEntry->objData->value.objId = objData->value.objId;
		break;
	
	case 'a':	// ObjId Array
		// need more info to allocate memory for this array of IdArray (long [])
		// size of array need to be known here to do dynamic allocation. 
		// idArray is a pointer to long. could consider adding the size of the array to the first element of this array before passing the array name as argument.
		// the reason is after passed as argument, the array is decayed to just a pointer. sizeof(arrayneme) no longer gives the size of the entire array, but only the single first element.
		break;
		
	};
*/	

	newEntry->objId = objId;
	newEntry->objData = objData;
	newEntry->nextVisibleScope = nextVisibleScope;
	
	// push the new stack entry
	
	newEntry->next = env->top;
	env->top = newEntry;
	env->depth++; 
	return TRUE;
}



/*
 * Adds a new element to the top of result stack, returns TRUE if success, False otherwise.
 */

Boolean pushRes(Res *res, ObjId objId, StackElement *objData) {
	
	// create a new stack entry
	
	ResEntry *newEntry;
	
	newEntry = (ResEntry *)malloc(sizeof(ResEntry));
	if (newEntry == NULL) {
		printf("pushRes(): can not allocate memory for newEntry.\n");
		return FALSE;
	}

	newEntry->objData = (StackElement *)malloc(sizeof(StackElement));
	if (newEntry->objData == NULL) {
		printf("pushRes(): can not allocate memory for newEntry->objData.\n");
		return FALSE;
	}
	
	newEntry->objId = objId;
		
	newEntry->objData->type = objData->type;   
	
	switch(newEntry->objData->type) {
	
	case 'b':	// basic
		newEntry->objData->value.bvalue = (String)malloc(sizeof(objData->value.bvalue) + 1);
		if (newEntry->objData->value.bvalue == NULL) {
			printf("pushEnv(): can not allocate memory for newEntry->objData->value.bvalue\n");
			return FALSE;
		}
		
		strcpy(newEntry->objData->value.bvalue, objData->value.bvalue);
		break;
	
	case 'p':	// pointer
		newEntry->objData->value.ptr = objData->value.ptr;
		break;
	
	case 'i':	// ObjId
		newEntry->objData->value.objId = objData->value.objId;
		break;
	
	case 'a':	// ObjId Array
		// need more info to allocate memory for this array of IdArray (long [])
		// size of array need to be known here to do dynamic allocation. 
		// idArray is a pointer to long. could consider adding the size of the array to the first element of this array before passing the array name as argument.
		// the reason is after passed as argument, the array is decayed to just a pointer. sizeof(arrayneme) no longer gives the size of the entire array, but only the single first element.
		break;
		
	};
	
	
	// push the new stack entry
	
	newEntry->next = res->top;
	res->top = newEntry;
	//res->depth++; 
	return TRUE;
}



/*
 * Returns the top element on the environment stack. 
 * Caller's responsibility to make sure the stack is not empty before calling.
 */

EnvEntry topEnv(Env *env) {
	
	if (emptyEnv(env)) {
		printf("topEnv(): stack is empty.\n");
		exit(1);
		//return ? if the return type is EnvEntry *, can return 0 here.
	}
	
	return *(env->top);
}


/*
 * Returns the top element on the result stack. 
 * Caller's responsibility to make sure the stack is not empty before calling.
 */

ResEntry topRes(Res *res) {
	
	if (emptyRes(res)) {
		printf("topRes(): stack is empty.\n");
		exit(1);
		//return ? if the return type is ResEntry *, can return 0 here.
	}
	
	return *(res->top);
}

/*
 * Removes the top element from environment stack. 
 * Returns TRUE if success, FALSE otherwise.
 */

Boolean popEnv(Env *env) {
	
	EnvEntry *temp;
	
	if (emptyEnv(env)) {
		printf("popEnv(): stack is empty.\n");
		return FALSE;
	}
	
	temp = env->top->next;

/* comment out since environment stack only stores pointers to objects.
 
	// free up all memory allocated for this top entry, in reverse order.
	
	if (env->top->objData->type == 'b') {
		free((void *)(env->top->objData->value.bvalue));
	}
	// if type is 'a', need to release memory for the array as well. later.
	// 
	free((void *)env->top->objData);
	free((void *)env->top);
*/	
	env->top = temp; 
	env->depth--;
	return TRUE;
}


/*
 * Removes the top element from result stack. 
 * Returns TRUE if success, FALSE otherwise.
 */

Boolean popRes(Res *res) {
	
	ResEntry *temp;
	
	if (emptyRes(res)) {
		printf("popRes(): stack is empty.\n");
		return FALSE;
	}
	
	temp = res->top->next;

	// free up all memory allocated for this top entry, in reverse order.
	
	if (res->top->objData->type == 'b') {
		free((void *)(res->top->objData->value.bvalue));
	}
	// if type is 'a', need to release memory for the array as well. later.
	// 
	free((void *)res->top->objData);
	free((void *)res->top);
	
	res->top = temp; 
	//res->depth--;
	return TRUE;
}
