
/*

1. Circular Queue operations.Queue element is different from result stack entry, logic is the same.

2. Test pipelining techniques.

x. Aim of parallelism: better performance. So,

   . How database files are distributed matters a lot.
   . RPC and MP those tasks that require lots of computation and diskaccess but little communication.
   . Pthread the rest.
   . ==-=====--===  optimise the bottlenecks


*/

#include <stdio.h>
#include <pthread.h>

#define QUEUE_MAX	100

#define NAME_MAX	20
#define LINE_MAX	100	// max length of a Record line in txt file.

typedef struct RecordTag {
	unsigned long	id;
	char			name[NAME_MAX];
	char			sex; // m, f.
} Record;


Record	queue[QUEUE_MAX];
long	qHead = -1;
long	qTail = -1;
long 	qLen = 0;

pthread_mutex_t queueMutex;

int scanN = 0, sortN = 0;


char	scanFlag = 1;


char emptyQ(void) {
	return qLen == 0 ? 1:0;
}


char fullQ(void) {
	return qLen == QUEUE_MAX ? 1:0;
}

char enQ(Record *e) {

	long i;
	long t;


	if (fullQ()) {
		return 0;
	}

	if (emptyQ()) {
		qHead = 0;
		qTail = 0;
	} else {
		if (qTail == QUEUE_MAX - 1) {
			qTail = 0;		// circle back
		} else {
			qTail++;		// still has palces
		}
	}


	//t = qLen;
 	//for (i = 0; i < 100000; i++) ;
	//qLen=t + 1;
	qLen++;	//atomic
	queue[qTail] = *e;
	printf("enQ(): %u|%-20s|%c  l=%d h=%d t=%d \n", e->id, e->name, e->sex, qLen, qHead, qTail);
	return 1;
}


Record deQ(void) {

	int head;

	if (emptyQ()) {
		printf("deQ(): queue is empty.\n");
		exit(1);
	} else {
		head = qHead;
		if (qHead == QUEUE_MAX - 1) {
			qHead = 0;
		} else {
			qHead++;
		}
	}

	qLen--;
	if (qLen == 0) {
		qHead = -1l;
		qTail = -1l;
	}
	return queue[head];
}



/*
Thread function.

pull Records out of queue (into local buffer), start sort when get SOME_MAX numbers of records,
output sort result to temporary files.
*/
void *sort(void *file) {
	Record e;

	while (scanFlag || !emptyQ()) {	// poll till there is no more data coming up
		//printf(".....\n");
		if(!emptyQ()) {
			pthread_mutex_lock(&queueMutex);
			e = deQ();
			sortN++;
			printf("\tdeQ(): %u|%-20s|%c  l=%d h=%d t=%d \n", e.id, e.name, e.sex, qLen, qHead, qTail);
			pthread_mutex_unlock(&queueMutex);


			// if SOME_MAX numbers of records is available,
			// sort and output to temporary files (could be done in another thread).


		}
	}
	printf("scanN: %d sortN: %d\n", scanN, sortN);
}



/*
Thread function.

scan the specified file, push the records into the queue as long as it is not full,
exit when reaching the end of the file.
*/
void *scan(void *file) {

	Record e;
	unsigned long id;
	char name[NAME_MAX];
	char sex;
	char s[1];

	FILE *fp;
	char *line;
	char *ptrptr;	// last argument to strtok_r()

	pthread_t t2;

	printf("creating thread sort() from t1\n");
	pthread_create( &t2, NULL, sort, file);

	fp = fopen((char *)file, "r");
	if (fp == NULL) {
		printf("can not open \"%s\"\n", (char*)file);
		exit(1);
	}

	line = (char *)malloc(sizeof(char) * LINE_MAX);
	ptrptr = (char *)malloc(sizeof(char) * LINE_MAX);

	while (fgets(line, LINE_MAX, fp) != NULL) {
		//printf("%s ", line);
		e.id = (unsigned long)atoi(strtok_r(line, "|", &ptrptr));
		strcpy(e.name, strtok_r(NULL, "|", &ptrptr));
		strcpy(s, strtok_r(NULL, "|", &ptrptr));
		e.sex = s[0];
		//printf("..%u|%s|%c.\n", e.id, e.name, e.sex);

		while(1) {		// poll till queue is not full.
			if (!fullQ()) {
				pthread_mutex_lock(&queueMutex);	// without mutex locks, program is faster
				enQ(&e);							// not always. test more..
				scanN ++;
				pthread_mutex_unlock(&queueMutex);
				break;
			}
		}
	}

	scanFlag = 0;
	fclose(fp);
	pthread_exit(NULL);
}


int main() {

	Record e;
	Record r;

	long l;

	pthread_t t1;
	//int tid1 = 1;
	int rc, status;

	char *filename = "a.txt";

/*
	printf("%d \n", qHead);

	e.id = 1000;
	strcpy(e.name,"1000name");
	e.sex = 'm';

	printf("0 %d \n", enQ(&e));

	e.id = 1001;
	strcpy(e.name,"1001name");
	e.sex = 'm';

	printf("1 %d \n", enQ(&e));

	e.id = 1002;
	strcpy(e.name,"1002name");
	e.sex = 'm';

	printf("2 %d \n", enQ(&e));

	e.id = 1003;
	strcpy(e.name,"1003name");
	e.sex = 'm';

	printf("3 %d \n", enQ(&e));


	r = deQ();
	printf("4 %u, %s, %c, l=%u h=%u t=%u \n", r.id, r.name, r.sex, qLen, qHead, qTail);

	e.id = 1004;
	strcpy(e.name,"1004name");
	e.sex = 'm';

	printf("5 %d \n", enQ(&e));

	r = deQ();
	printf("6 %u, %s, %c, l=%u h=%u t=%u \n", r.id, r.name, r.sex, qLen, qHead, qTail);
	r = deQ();
	printf("7 %u, %s, %c, l=%u h=%u t=%u \n", r.id, r.name, r.sex, qLen, qHead, qTail);
	r = deQ();
	printf("8 %u, %s, %c, l=%u h=%d t=%d \n", r.id, r.name, r.sex, qLen, qHead, qTail);

	//r = deQ();
*/




	pthread_mutex_init(&queueMutex, NULL);

	printf("creating thread scan() \n");
	pthread_create( &t1, NULL, scan, filename);

	//printf("creating thread sort() \n");
	//pthread_create( &t2, NULL, sort, filename);




	//scan((void *)"a.txt");

	rc = pthread_join(t1, (void **)&status);
	if (rc){
		printf("ERROR return code from pthread_join() is %d\n", rc);
		exit(-1);
	}

	pthread_mutex_destroy(&queueMutex);
	pthread_exit(NULL);
	return 0;
}



