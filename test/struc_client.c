/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "struc.h"


void
master_1(char *host)
{
	CLIENT *clnt;
	Queue  *result_1;
	char  prime_1_arg;

#ifndef	DEBUG
	clnt = clnt_create (host, MASTER, MASTERVER, "udp");
	if (clnt == NULL) {
		clnt_pcreateerror (host);
		exit (1);
	}
#endif	/* DEBUG */

	result_1 = prime_1(&prime_1_arg, clnt);
	if (result_1 == (Queue *) NULL) {
		clnt_perror (clnt, "call failed");
	}
#ifndef	DEBUG
	clnt_destroy (clnt);
#endif	 /* DEBUG */
}


int
main (int argc, char *argv[])
{
	char *host;

	if (argc < 2) {
		printf ("usage: %s server_host\n", argv[0]);
		exit (1);
	}
	host = argv[1];
	master_1 (host);
exit (0);
}
