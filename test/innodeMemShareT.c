

/*

can i share memory space between pvm tasks running on the same node?

don't forget to test it on SPARC.

*/


#include <stdio.h>
#include <pvm3.h>


int main(void) {
	int mytid, ptid;
	int cc;
	int *lp;
	FILE *fp;

	fp = fopen("/home/dagong/ca/err", "w");	// redirect error messages
	pvm_catchout(fp);

	fprintf(fp, "in child task\n");
	ptid = pvm_parent();

	fprintf(fp, "task(): %x is son of %x\n", pvm_mytid(), ptid);

	cc = pvm_recv(ptid, -1);
	//pvm_bufinfo(cc, (int*)0, (int*)0, &tid);
	pvm_upkint(lp, 1, 1);

	printf("task(); %d", *lp);

	pvm_exit();
	fclose(fp);
	exit(0);

}



