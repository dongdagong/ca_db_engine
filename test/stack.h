
/*
 * File: stack.h
 * Data structure and operation declarations for Environment Stack and Result Stack.
 */


#include "common.h"

typedef unsigned char	Type;
typedef char *			String;
typedef void *			Pointer;
typedef unsigned long	ObjId;
typedef unsigned long * IdArray;


// Stack Element Type, same for both Result stack and Environment stack.

typedef struct {

	Type type;
	union {
		String 	bvalue;		// 'b'
		Pointer ptr;		// 'p'
		ObjId	objId;		// 'i'
		IdArray	idArray;	// 'a'
	} value;

} StackElement;


// Environment Stack Entry Type.

typedef struct EnvEntryTag {

	ObjId 			objId;
	StackElement 		*objData;

	struct EnvEntryTag 	*next;
	struct EnvEntryTag 	*nextVisibleScope;

	// additional pointers;
} EnvEntry;


// Result Stack Entry Type. 

typedef struct ResEntryTag {
	
	ObjId 			objId;
	StackElement 		*objData;
	
	struct ResEntryTag 	*next;

	// additional pointers;
} ResEntry;


// Environment Stack Type.

typedef struct EvnTag {

	unsigned long	depth;	// reserved.
	EnvEntry 	*top;

	// additional pointers
	
} Env;


// Result Stack Type.

typedef struct ResTag {

	ResEntry 	*top;
	ResEntry	*pipeFrom;

	// additional pointers
	
} Res;




/*
 * Initialize a environment stack.
 */

Env *initEnv(void);

/*
 * Initialize a Result stack.
 */

Res *initRes(void);


/*
 * Return TRUE if the stack is empty, FALSE otherwise.
 * Only should be called after initEnv.
 */

Boolean emptyEnv(Env *);

/*
 * Return TRUE if the stack is empty, FALSE otherwise.
 * Only should be called after initEnv.
 */

Boolean emptyRes(Res *s);


/*
 * Adds a new element to the top of environment stack, returns TRUE if success, False otherwise.
 */

Boolean pushEnv(Env *env, ObjId objId, StackElement *objData, EnvEntry *nextVisibleScope);


/*
 * Adds a new element to the top of result stack, returns TRUE if success, False otherwise.
 */

Boolean pushRes(Res *res, ObjId objId, StackElement *objData);


/*
 * Returns the top element on the environment stack.
 * Caller's responsibility to make sure the stack is not empty before calling.
 */

EnvEntry topEnv(Env *env);


/*
 * Returns the top element on the result stack.
 * Caller's responsibility to make sure the stack is not empty before calling.
 */

ResEntry topRes(Res *res);


/*
 * Removes the top element from environment stack.
 * Returns TRUE if success, FALSE otherwise.
 */

Boolean popEnv(Env *env);


/*
 * Removes the top element from result stack.
 * Returns TRUE if success, FALSE otherwise.
 */

Boolean popRes(Res *res);
