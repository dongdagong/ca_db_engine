

#include <stdio.h>
#include <pvm3.h>
#include <pthread.h>

#define MAX 10000

unsigned long cc = 0;
pthread_mutex_t m;


void *threadFunc(void *v) {

	int *tid;
	unsigned long i;
	int j;
	unsigned long t;

	tid = (int *)v;

	for (i = 0; i < MAX; i++) {
		printf("%d %u \n", *tid, cc);
		//pthread_mutex_lock (&m);
		t = cc;
		for (j = 0; j < 1000; j++) ;
		//cc = cc + 1; // seems to be an atomic operation.
		cc = t + 1;
		//pthread_mutex_unlock (&m);
	}


	pthread_exit(NULL);
}



int main(void) {

	pthread_t t1, t2;
	int tid1 = 1, tid2 = 2;
	int rc, status;


	pthread_mutex_init(&m, NULL);

	printf("creating t1 \n");
	pthread_create( &t1, NULL, threadFunc, &tid1);

	printf("creating t2 \n");
	pthread_create( &t2, NULL, threadFunc, (void *)&tid2);

	pthread_mutex_destroy(&m);



      rc = pthread_join(t1, (void **)&status);
      if (rc)
      {
         printf("ERROR return code from pthread_join() is %d\n", rc);
         exit(-1);
      }
      printf("Completed join with thread %d status= %d\n",tid1, status);

      rc = pthread_join(t2, (void **)&status);
      if (rc)
      {
         printf("ERROR return code from pthread_join() is %d\n", rc);
         exit(-1);
      }
      printf("Completed join with thread %d status= %d\n",tid2, status);

	printf("cc = %u \n", cc);

	pthread_exit(NULL);
	return 0;



}

