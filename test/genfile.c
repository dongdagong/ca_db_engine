
/*
Generate test text files.
*/

#include <stdlib.h>
#include <stdio.h>


void genName(char *name) {
	int i;
	int len;
	char c;

	free(name);
	name = (char *)malloc(sizeof(char) * 20);

	len = (float)rand()/RAND_MAX * 10 + 10; 	// [10, 20)
	for (i = 0; i < len; i++) {
		c = (float)rand()/RAND_MAX * 26 + 97;
		name[i] = c;
		//char *itoa(int value, char *string, int radix);
	}
	name[i] = '\0';

	//printf("%c %c\n", 97, 122);
	//printf("%d %s \n", len, name);
}

int main(int argc, char **argv) {

	FILE *fp;
	char *fn = "a.txt";

	unsigned long l;
	unsigned long id;
	char *name;
	char sex;
	char *line;

	name = (char *)malloc(sizeof(char) * 20);
	line = (char *)malloc(sizeof(char) * 100);

	fp = fopen(fn, "w");
	if (fp == NULL) {
		printf("can not open %s \n", fn);
		exit(1);
	}

	for (l = 0; l < 1000; l++) {
		id = l;
		genName(name);
		sex = rand() % 2 == 0 ? 'f':'m';

		sprintf(line, "%u|%s|%c\n",id, name, sex);

		//printf("%s \n", line);
		fputs(line, fp);
	}

	fclose(fp);
	return 0;
}






