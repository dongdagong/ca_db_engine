
#include "ma.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


void init_sockaddr (struct sockaddr_in *sa, char *hostname, uint16_t port) {
	int i;
	
	sa->sin_family = AF_INET;
	sa->sin_port = htons (port);
	memset((sa->sin_zero), '\0', 8);
	i =  inet_aton(hostname, &(sa->sin_addr));
	
	printf("sin_port %u \n", sa->sin_port);
	printf("port %d \n", ntohs(sa->sin_port));
	printf("inet_aton returns: %d \n", i);
	printf("sa->sinaddr.s_addr = %u \n", sa->sin_addr.s_addr);
	printf("host: %s \n", inet_ntoa(sa->sin_addr));
}

void masteragent_1(char *host, uint16_t port)
{
	CLIENT *clnt;
	MCARPCListener  *result_1;
	char * rpc_ma_1_arg = "select * and a bit more";

	struct sockaddr_in *sa;


	printf("initializing  sa \n");

	init_sockaddr(sa, host, port);

	printf("creating clnt \n");

	//clnt = clnt_create (host, MASTERAGENT, VERSION1, "tcp");

	clnt = clnttcp_create(sa, MASTERAGENT, VERSION1, 32863, 0, 0); //RPC_ANYSOCK, 0, 0);

	printf("create clnt finished \n");
	
	if (clnt == NULL) {
		clnt_pcreateerror (host);
		exit (1);
	} else
		printf("clnt is not null");


	result_1 = rpc_ma_1(&rpc_ma_1_arg, clnt);
	if (result_1 == (MCARPCListener *) NULL) {
		clnt_perror (clnt, "call failed");
	}

	printf("%s %d \n", result_1->host, result_1->progNum);

	clnt_destroy (clnt);

}


int
main (int argc, char *argv[])
{

	masteragent_1 ("130.123.33.158", (uint16_t)32863);
	exit (0);
}
