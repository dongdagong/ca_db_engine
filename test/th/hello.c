/******************************************************************************
* FILE: hello.c
* DESCRIPTION:
*   A "hello world" Pthreads program.  Demonstrates thread creation and
*   termination.
*
* SOURCE:
* LAST REVISED: 9/20/98 Blaise Barney
******************************************************************************/

#include <pthread.h>
#include <stdio.h>
#define NUM_THREADS	5

	int *ip;


void *PrintHello(void *threadid)
{
	int j;
	j++;
   printf("in thread: %d\n", *(int *)threadid);
   pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
   pthread_t threads[NUM_THREADS];
   int rc, t;

	ip = (int *)malloc(sizeof(int));

	*ip = 555;

   for(t=0;t<NUM_THREADS;t++){
      printf("in main: %d\n", t);
      rc = pthread_create(&threads[t], NULL, PrintHello, (void *)ip);

      if (rc){
         printf("ERROR; return code from pthread_create() is %d\n", rc);
         exit(-1);
      }
   }
   pthread_exit(NULL);
}
