#include <stdio.h>

#define MAX 3
#define STR "this is a string hehe:) ......................."
/*
iIlL.
.....
oO0
sony-fixed font
*/

typedef struct {

	int k;
	union {
		char * s;
		unsigned long *l;
	} u;
} Struct2;

typedef struct {
	int i;
	int j;
	Struct2 *str;
} Struct;

void fun1(Struct *s) {
	s->str = NULL;
	s->i = 5;
}

char empty(Struct *s) {
	return (s->str == NULL);
}


void f3(long lon[]) {
	int i;
	i = sizeof(char[6]);

	printf("main: long[]   %d %d\n", sizeof(lon), sizeof(lon[0]));

	for (i = 0; i < 6; i++)
		printf("f3: *l++ = %u \n", lon[i]);
}

void f32(Struct2 *str2) {
	printf("f32: %d", sizeof(str2->u.l));
}


int main(int argc, char **argv) {	//, char **envp) {

	unsigned long l;
	char *heap[MAX];

	// 2------------------------------------------------
	Struct s;

	// 3 --------------------------------------------
	unsigned long lon[] = {11, 2, 3, 4, 5, 6};
	Struct2 struct2[13];
	Struct2 str2;

	// 4 ---------------------------------------------

	int num;
	unsigned long i, j;
	unsigned long a, b;
	//int p;

	// 5 -----------------------------------------------

	FILE *fp;

	// 6 -----------------------------------------------
	char envname[10] = "HOST";



	////////////////////////////// operation /////////////////////////////

	// acquire memory
	for (l = 0; l < MAX; l++) {
		heap[l] = (char *)malloc(sizeof(STR) + 1);
		printf("%d \n", l);
		strcpy(heap[l], STR);
	}

	// free memory
	for (l = MAX - 1; l > 0 ; l--) {
		int k;
		k = 0;
		printf("%d \n", l);
		free((void *)heap[l]);
	}

	printf(" ----------------- \n");



	// 2 ------------------------------------------------------
	s.i = 1;
	fun1(&s);
	printf("s.i = %d \n", s.i);
	printf("%d\n", empty(&s));

	// 3----------------------------------------------
	f3(lon);
	printf("main: struct[] %d %d\n", sizeof(struct2), sizeof(struct2[0]));
	printf("main: long[]   %d %d\n", sizeof(lon), sizeof(lon[0]));


	// 4 ---------------------------

	printf("\n\n 4 ----------\n\n");

	printf("%u \n", 0l / 1l);

	num = 0;
	if (argc == 3) {
		a = atoi(argv[1]);
		b = atoi(argv[2]);
	} else {
		a = 0;
		b = 10;
	}

	printf("a %u  b %u \n", a, b);

	for (i = a; i <= b; i++) {	// 4 seconds for 1000 000

		// test if number i is prime.
		int p;
		p = 1;
		for (j = 2; (j*j) <= i; j++) {

			//printf("i:%u j:%u i mod j:%u \n", i, j, i%j);

			if (i % j == 0) {

				p = 0;
				break;
			}
		}

		if (p && i > 2) {
			num++;

			//printf("%u\n", i);
		}
	}

	printf("num of primes = %d\n", num);


	// 5 -----------------------------------------------
	printf("\n\n 5----------\n\n");

	fp = fopen("file1", "a");
	fprintf(fp, "looking into the spyglass line 1\n");
	fprintf(fp, "jumping across the universe boundary. (if there is one)\n");

	fclose(fp);

	// 6 -----------------------------------------------
	printf("\n\n 6----------\n\n");

	printf("env variables - %s \n", getenv(envname));
	printf("%d \n", strcmp("it001930", 
	getenv(envname)));




	return 0;
}

