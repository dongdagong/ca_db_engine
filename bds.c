
/*

file: bds.c

Basic Data Structures and operations.

*/

#include <stdio.h>
#include <pthread.h>
#include "bds.h"


///////////////////////////////// Object /////////////////////////////////////////

// Creates a new Object

Object *objNewB(Type type, String name, String bvalue) {

	Object	*o;

	o = (Object *)malloc(sizeof(Object));

	o->type = type;	// B S D I
	o->name = name == NULL ? NULL : (char *)strdup(name);
	o->value.bvalue = (char *)strdup(bvalue);
	if (o->value.bvalue == NULL) printf("objNewB: bvalue is NULL\n");
	// without the above 'if', segmentation fault

	return o;
}

Object *objNewP(String name, Pointer ptr) {

	Object	*o;

	o = (Object *)malloc(sizeof(Object));

	o->type = 'P';	// pointer
	o->name = name == NULL ? NULL : (char *)strdup(name);
	o->value.ptr = ptr;

	return o;
}

Object *objNewO(String name, ObjId objId) {

	Object	*o;

	o = (Object *)malloc(sizeof(Object));

	o->type = 'O';	// object id
	o->name = name == NULL ? NULL : (char *)strdup(name);
	o->value.objId = objId;

	return o;
}

// Constructs a new Object from the given string of the form "type:name:value"

Object *objNew(char *tripleSrc) {
	Object 	*o;
	char	*typeStr;
	char 	*name;
	char 	*valueStr;
	Type	type;
	char 	*triple;
	char 	*buf;

	triple = (char *)strdup(tripleSrc);

	//printf("%s %s\n", tripleSrc, triple);

	buf = (char *)malloc(sizeof(char) * 100);
	typeStr = (char *)malloc(sizeof(char) * strlen(triple));
	name = (char *)malloc(sizeof(char) * strlen(triple));
	valueStr = (char *)malloc(sizeof(char) * strlen(triple));

	typeStr = (char *)strtok_r(triple, ":", &triple);
	name = (char *)strtok_r(NULL, ":", &triple);
	valueStr = (char *)strtok_r(NULL, ":", &triple);

	if (strcmp(name, "NULL") == 0)
		name = NULL;

	type = *typeStr;

	//printf("t=%s n=%s v=%s t=%c\n", typeStr, name, valueStr, type);

	switch (type) {
	case 'B': case 'S': case 'D': case 'I':
		o = objNewB(type, name, valueStr);
		break;
	case 'P':
		// o = objNewP(name, src->value.ptr);
		break;
	case 'O':
		o = objNewO(name, strtoul(valueStr, NULL, 10));
		break;
	}

	return o;
}

// Clones this object

Object *objClone(Object *src) {

	Object		*dest;

	switch (src->type) {
	case 'B': case 'S': case 'D': case 'I':
		dest = objNewB(src->type, src->name, src->value.bvalue);
		break;
	case 'P':
		dest = objNewP(src->name, src->value.ptr);
		break;
	case 'O':
		dest = objNewO(src->name, src->value.objId);
		break;
	}

	return dest;
}

Boolean objComp(Object *o1, Object *o2) {
	if (o1->type != o2->type)
		return FALSE;
	if (strcmp(o1->name, o2->name) != 0)
		return FALSE;
	switch (o1->type) {
	case 'B': case 'S': case 'D': case 'I':
		if (strcmp(o1->value.bvalue, o2->value.bvalue) == 0)
			return TRUE;
		break;
	case 'P':
		if (o1->value.ptr == o2->value.ptr)
			return TRUE;
		break;
	case 'O':
		if (o1->value.objId == o2->value.objId)
  			return TRUE;
		break;
	}
	return FALSE;
}


// Frees memory space allocated for this Object.

void objFree(Object *o) {

	free(o->name);

	switch(o->type) {
		case 'B': case 'S': case 'D': case 'I':
			free(o->value.bvalue);
			break;
		case 'P':
			// ?
			break;
	}

	free(o);
}

// Displays the content of this Object.

void objDisp(Object *o) {

	printf("\t> %c:%s:", o->type, o->name);

	switch (o->type) {
	case 'B': case 'S': case 'D': case 'I':
		printf("%s\n", o->value.bvalue);
		break;
	case 'P':
		printf("%p\n", o->value.ptr);
		break;
	case 'O':
		printf("%d\n", o->value.objId);
		break;
	}
}

////////////////////////// Object List /////////////////////////////////////////

// Initializes an Object List

OL *olInit(void) {
	OL *ol;

	ol = (OL *)malloc(sizeof(OL));
	if (ol == NULL) {
		printf("olInit(): out of memory\n");
		return NULL;
	}
	ol->first = NULL;
	ol->last = NULL;
	ol->p = NULL;
	ol->n = NULL;
	ol->len = 0;
	return ol;
}


// Returns TRUE if the object list is empty, returns FALSE otherwise.

Boolean olIsEmpty(OL *ol) {
	return (ol->len == 0);
}



// Wraps this object into an OLEntry and adds it after the last entry in this list.

Boolean olAdd(OL *ol, Object *obj) {

	OLEntry		*e;

	e = (OLEntry *)malloc(sizeof(OLEntry));
	if (e == NULL) {
		printf("olAdd(): out of memory\n");
		return FALSE;
	}

	e->obj = objClone(obj);
	e->next = NULL;
	e->prev = ol->len == 0 ? NULL : ol->last;

	if (ol->len == 0) {
		ol->first = e;
	} else {
		ol->last->next = e;
	}

	ol->last = e;
	ol->len++;

	return TRUE;
}

// Returns a pointer to the object contained in the first entry of this object list.

Object	*olGet(OL *ol) {

	if (!olIsEmpty(ol))
		return (ol->first->obj);
	return NULL;
}


void olInitIterator(OL *ol) {
	ol->p = NULL;
	ol->n = ol->first;
}

Boolean olHasNext(OL *ol) {
	return ol->n == NULL ? FALSE : TRUE;
}

Object *olNext(OL *ol) {

	ol->p = ol->n;
	ol->n = ol->n->next;

	return ol->p->obj;
}


// Removes the entry returned by olNext(), ol->p.

void olRemoveCur(OL *ol) {



	if (ol->len == 1) {
		objFree(ol->p->obj);
		ol->first = NULL;
		ol->last = NULL;
		ol->p = NULL;
		ol->n = NULL;
	} else {
		if (ol->p == ol->first) {		// removing the first entry
			ol->first = ol->n;
			ol->n->prev = NULL;
		} else if (ol->n == NULL) {		// removing the last entry
			ol->last = ol->p->prev;
			ol->last->next = NULL;
		} else {						// removing an entry in the middle
			ol->p->prev->next = ol->n;
			ol->n->prev = ol->p->prev;
		}
		objFree(ol->p->obj);
	}

	ol->len--;
/*
	if (ol->p == ol->first) {		// removing the first entry
		ol->first = ol->n;
		ol->n->prev = NULL;
	} else if (ol->n == NULL) {		// removing the last entry
		ol->last = ol->p->prev;
		ol->last->next = NULL;
	} else {						// removing an entry in the middle
		ol->p->prev->next = ol->n;
		ol->n->prev = ol->p->prev;
	}

	ol->len--;
	objFree(ol->p->obj);
*/

}


// Removes the first entry of this object list

Boolean olRemove(OL *ol) {
	OLEntry *temp;

	if (!olIsEmpty(ol)) {

		temp = ol->first;
		ol->first = temp->next;
		if (ol->first != NULL)
			ol->first->prev = NULL;

		ol->len--;

		objFree(temp->obj);
		free(temp);
		return TRUE;
	}
}


// Clones this object list

OL *olClone(OL *src) {

	OL		*dest = NULL;
	OLEntry	*temp;

	dest = olInit();

	if (!olIsEmpty(src)) {

		temp = src->first;

		while (temp->next != NULL) {

			olAdd(dest, temp->obj);		// object cloned in olAdd()
			temp = temp->next;
		}

		olAdd(dest, temp->obj);
	}

	return dest;
}


// Frees memory space allocated for this Object List

void olFree(OL *ol) {

	if (ol == NULL)
		return;

	while (!olIsEmpty(ol)) {
		olRemove(ol);
	}

	//free(ol);
}

// Displays the content of this Object List

void olDisp(OL *ol) {

	OLEntry	*temp;

	printf("[OL]:  len = %d \n", ol->len);

	if (!olIsEmpty(ol)) {

		temp = ol->first;

		while (temp->next != NULL) {
			objDisp(temp->obj);
			temp = temp->next;
		}

		objDisp(temp->obj);
	}
	printf("\n");
	//printf("This Object List is empty\n");
}

///////////////////////////// Stacks //////////////////////////////////////////

// Constructs a new ESEntry based on the given data, no clone for object list.

ESEntry *esNewEntry(ObjId objId, Type type, String name, OL *ol) {

	ESEntry *newEntry;
	ESEntry *nextVisibleScope = NULL;

	newEntry = (ESEntry *)malloc(sizeof(ESEntry));
	if (newEntry == NULL) {
		printf("esNewEntry(): out of memory\n");
		return NULL;
	}

	// ES stores pointers only, so do not malloc pointers

	newEntry->objId = objId;
	newEntry->type = type;
	newEntry->name = name == NULL ? NULL : (char *)strdup(name);
	newEntry->ol = ol;

	newEntry->nextVisibleScope = nextVisibleScope;

	return newEntry;
}

// Constructs a new RSEntry based on the given data, no clone of object list.

RSEntry *rsNewEntry(ObjId objId, Type type, String name, OL *ol) {

	RSEntry *newEntry;

	newEntry = (RSEntry *)malloc(sizeof(RSEntry));
	if (newEntry == NULL) {
		printf("rsNewEntry(): out of memory\n");
		return NULL;
	}

	newEntry->objId = objId;
	newEntry->type = type;
	newEntry->name = name == NULL ? NULL : (char *)strdup(name);
	newEntry->ol = ol;

	return newEntry;
}


// Clones this Environment Stack Entry

ESEntry *eseClone(ESEntry *src) {

	ESEntry	*dest;

	dest = (ESEntry *)malloc(sizeof(ESEntry));
	if (dest == NULL) {
		printf("eseClone(): out of memory\n");
		return NULL;
	}

	dest->objId = src->objId;
	dest->type = src->type;
	dest->name = (char *)strdup(src->name);
	dest->ol = olClone(src->ol);
	dest->next = src->next;
	dest->nextVisibleScope = src->nextVisibleScope;

	return dest;
}


// Clones this Result Stack Entry

RSEntry *rseClone(RSEntry *src) {

	RSEntry	*dest;

	dest = (RSEntry *)malloc(sizeof(RSEntry));
	if (dest == NULL) {
		printf("rseClone(): out of memory\n");
		return NULL;
	}

	dest->objId = src->objId;
	dest->type = src->type;
	dest->name = src->name == NULL ? NULL : (char *)strdup(src->name);
	dest->ol = olClone(src->ol);
	dest->next = src->next;

	return dest;
}

// Frees memory allocated for this Environment Stack Entry;

void eseFree(ESEntry *e) {
	free(e->name);
	//olFree(e->ol);
	free(e);
}


// Frees memory allocated for this Result Stack Entry;

void rseFree(RSEntry *e) {
	free(e->name);
	olFree(e->ol);
	free(e);
}

// Displays content of this Environment Stack Entry

void eseDisp(ESEntry *e) {
	printf("  %d\t%-10s\t%c\t", e->objId, e->name, e->type);
	olDisp(e->ol);
}


// Displays content of this Result Stack Entry

void rseDisp(RSEntry *e) {
	printf("  %d\t%-10s\t%c\t", e->objId, e->name, e->type);
	olDisp(e->ol);
}



// Initializes an environment stack.

ES *esInit(void) {

	ES *s;

	s = (ES *)malloc(sizeof(ES));
	if (s == NULL) {
		printf("esInit(): out of memory\n");
		return NULL;
	}

	s->top = NULL;
	s->depth = 0;

	return s;
}


// Initializes a Result stack.


RS *rsInit(void) {

	RS *s;

	s = (RS *)malloc(sizeof(RS));
	if (s == NULL) {
		printf("rsInit(): out of memory\n");
		return NULL;
	}
	s->top = NULL;

	s->depth = 0;
	return s;
}


// Returns TRUE if the stack is empty, FALSE otherwise.
// Only should be called after esInit().

Boolean esIsEmpty(ES *s) {

	return (s->top == NULL);
	//this works only on SPARC machine before changing to use *es instead of es in main(),
}


// Returns TRUE if the stack is empty, FALSE otherwise.
// Only should be called after rsInit().

Boolean rsIsEmpty(RS *s) {

	return (s->top == NULL);
}

// Adds a new entry to the top of environment stack, returns TRUE on success.
// Does not clone the entry before adding.

Boolean esPush(ES *es, ESEntry *newEntry) {

	newEntry->next = es->top;
	es->top = newEntry;
	es->depth++;

	return TRUE;
}


// Adds a clone of this entry to the top of result stack, returns TRUE on success.

Boolean rsPush(RS *rs, RSEntry *e) {

	RSEntry *newEntry;

	newEntry = rseClone(e);
	if (newEntry == NULL)
		return FALSE;

	newEntry->next = rs->top;
	rs->top = newEntry;
	rs->depth++;

	return TRUE;
}

Boolean rsPushTriple(RS *rs, ObjId objId, Type type, String name, char *triple) {
	Object	*o;
	OL	*ol;

	o = objNew(triple);
	ol = olInit();
	olAdd(ol, o);
	rsPush(rs, rsNewEntry(objId, type, name, ol));
	return TRUE;
}

// Returns the top element on the environment stack.
// Caller's responsibility to make sure the stack is not empty before calling.

ESEntry *esTop(ES *es) {

	if (esIsEmpty(es)) {
		printf("esTop(): stack is empty.\n");
		return NULL;
	}

	return es->top;
}

// Returns the top element on the result stack.
// Caller's responsibility to make sure the stack is not empty before calling.


RSEntry *rsTop(RS *rs) {

	if (rsIsEmpty(rs)) {
		printf("rsTop(): stack is empty.\n");
		return NULL;
	}

	return rs->top;
}


// Removes the top element from environment stack.
// Returns TRUE if success, FALSE otherwise.


Boolean esPop(ES *es) {

	ESEntry *temp;

	if (esIsEmpty(es)) {
		printf("esPop(): stack is empty.\n");
		return FALSE;
	}

	temp = es->top;
	es->top = temp->next;
	es->depth--;

	eseFree(temp);	// should free the whole Entry (including content) here?
					// because only pointer is pushed into this stack.
					// just free(temp), without freeing its contents?

					// pop means: remove the top element of the stack.

	return TRUE;
}


// Removes the top element from result stack.
// Returns TRUE if success, FALSE otherwise.


Boolean rsPop(RS *rs) {

	RSEntry *temp;

	if (rsIsEmpty(rs)) {
		printf("rsPop(): stack is empty.\n");
		return FALSE;
	}

	temp = rs->top;
	rs->top = temp->next;
	rs->depth--;

	rseFree(temp);

	return TRUE;
}

// Frees the content of this Environment Stack
// The Environment Stack itself is not freed.

void esFree(ES *s) {
	while (!esIsEmpty(s)) {
		esPop(s);
	}
}


// Frees the content of this Result Stack
// The Result Stack itself is not freed.

void rsFree(RS *s) {
	while (!rsIsEmpty(s)) {
		rsPop(s);
	}
}


// Displays the content of this Environment Stack

void esDisp(ES *s) {

	ESEntry	*temp;

	if (s != NULL) {
		printf("[ES]:  depth = %u  top = %p\n\n", s->depth, s->top);
		temp = s->top;
		if (temp != NULL) {
			while (temp->next != NULL) {
				eseDisp(temp);
				temp = temp->next;
			}
			eseDisp(temp);
		} else
			; //printf("ES top is NULL\n");
	} else
		printf("ES is NULL\n");
	printf("\n");
}

// Displays the content of this Result Stack

void rsDisp(RS *s) {

	RSEntry	*temp;

	if (s != NULL) {
		printf("[RS]:  depth = %u  top = %p\n\n", s->depth, s->top);
		temp = s->top;
		if (temp != NULL) {
			while (temp->next != NULL) {
				rseDisp(temp);
				temp = temp->next;
			}
			rseDisp(temp);
		} else
			; //printf("RS top is NULL\n");
	} else
		printf("RS is NULL\n");
	printf("\n");
}




//////////////////////////////// Queue /////////////////////////////////////////

// Constructs a new Result Queue Entry based on the given data
// No clone of object list

RQEntry *rqNewEntry(ObjId objId, Type type, String name, OL *ol) {

	RQEntry *newEntry;

	newEntry = (RQEntry *)malloc(sizeof(RQEntry));
	if (newEntry == NULL) {
		printf("rqNewEntry(): out of memory\n");
		return NULL;
	}

	newEntry->objId = objId;
	newEntry->type = type;

	newEntry->name = name == NULL ? NULL : (char *)strdup(name);
	newEntry->ol = ol;

	return newEntry;
}

// Clones this Result Queue Entry

RQEntry *rqeClone(RQEntry *src) {

	RQEntry *dest;

	dest = (RQEntry *)malloc(sizeof(RQEntry));
	if (dest == NULL) {
		printf("rqClone(): out of memory\n");
		return NULL;
	}

	dest->objId = src->objId;
	dest->type = src->type;
	dest->name = src->name == NULL ? NULL : (char *)strdup(src->name);
	dest->ol = olClone(src->ol);
	dest->next - src->next;

	return dest;
}


// Frees memory allocated for this Result Queue Entry;

void rqeFree(RQEntry *e) {
	free(e->name);
	olFree(e->ol);
	free(e);
}

// Displays content of this Result Queue Entry

void rqeDisp(RQEntry *e) {
	printf("  %d\t%-10s\t%c\t", e->objId, e->name, e->type);
	olDisp(e->ol);
}


// Initilizes this Result Queue

RQ *rqInit(unsigned long maxLen) {
	RQ *q;
	q = (RQ *)malloc(sizeof(RQ));
	if (q == NULL) {
		printf("rqInit(): Can not initialize result queue.\n");
		return NULL;
	}
	q->head = NULL;
	q->tail = NULL;
	q->len = 0;
	q->maxLen = maxLen;

	pthread_mutex_init(&(q->mutex), NULL);
	pthread_cond_init(&(q->avail), NULL);
	pthread_cond_init(&(q->ready), NULL);

	q->finish = 0;

	return q;
}

Boolean rqIsEmpty(RQ *rq) {
	return rq->len == 0;
}


Boolean rqIsFull(RQ * rq) {
	return rq->len == rq->maxLen;
}


// Puts a clone of this entry into the queue

Boolean rqEnQ(RQ *rq, RQEntry *e) {

	RQEntry *newEntry;

	newEntry = (RQEntry *)malloc(sizeof(RQEntry));
	if (newEntry == NULL) {
		printf("rqEnQ(): out of memory\n");
		return FALSE;
	}

	newEntry = rqeClone(e);

	pthread_mutex_lock(&rq->mutex);

	while (rqIsFull(rq)) {
 		pthread_cond_wait(&rq->ready, &rq->mutex);
	}

	if (rqIsEmpty(rq)) {
		rq->head = newEntry;
	} else {
		rq->tail->next = newEntry;
	}
	rq->tail = newEntry;
	newEntry->next = NULL;
	rq->len++;

	pthread_cond_signal(&rq->avail);
	pthread_mutex_unlock(&rq->mutex);

	return TRUE;
}



// Caller is responsible for freeing the returned entry.

RQEntry *rqDeQ(RQ *rq) {

	RQEntry *temp;

	pthread_mutex_lock(&rq->mutex);

	while (rqIsEmpty(rq)) {
 		pthread_cond_wait(&rq->avail, &rq->mutex);
	}

/*	if (rqIsEmpty(rq)) {
		printf("rqDeQ(): queue is empty.\n");
		return NULL;
	}
*/
	temp = rq->head;
	rq->head = temp->next;
	rq->len--;

	pthread_cond_signal(&rq->ready);
	pthread_mutex_unlock(&rq->mutex);

	return temp;
}


// Frees the content of this Result Queue
// The Result Queue itself is not freed.

void rqFree(RQ *rq) {

	RQEntry *rqe;

	while (!rqIsEmpty(rq)) {
		rqe = rqDeQ(rq);
		rqeFree(rqe);
	}

	pthread_mutex_destroy(&(rq->mutex));
	pthread_cond_destroy(&(rq->avail));
	pthread_cond_destroy(&(rq->ready));
}

// Displays the content of this Result Queue

void rqDisp(RQ *rq) {

	RQEntry	*temp;

	if (rq != NULL) {
		printf("[RQ]:  len = %u\n\n", rq->len);
		temp = rq->head;
		if (temp != NULL) {
			while (temp->next != NULL) {
				rqeDisp(temp);
				temp = temp->next;
			}
			rqeDisp(temp);
		} else
			; //printf("RQ head is NULL\n");
	} else
		printf("RQ is NULL\n");
	printf("\n");
}

//////////////////////////// Transformations //////////////////////////////////////


RQEntry *ese2rqe(ESEntry *ese) {
	RQEntry *rqe;
	rqe = rqNewEntry(ese->objId, ese->type, ese->name, ese->ol);
	return rqe;
}

RSEntry *rqe2rse(RQEntry *rqe) {
	RSEntry *rse;
	rse = rsNewEntry(rqe->objId, rqe->type, rqe->name, rqe->ol);
	return rse;
}

ESEntry	*rqe2ese(RQEntry *rqe) {
	return esNewEntry(rqe->objId, rqe->type, rqe->name, olClone(rqe->ol));
}

ESEntry *rse2ese(RSEntry *rse) {
	return esNewEntry(rse->objId, rse->type, rse->name, olClone(rse->ol));
}

RQEntry *rse2rqe(RSEntry *rse) {
	return rqNewEntry(rse->objId, rse->type, rse->name, rse->ol);
}


// Moves the content of RQ to RS

void rq2rs(RQ *rq, RS*rs) {
	RQEntry	*rqe;
	RSEntry *rse;

	while (!rqIsEmpty(rq)) {
		rqe = rqDeQ(rq);
		rse = rqe2rse(rqe);
		rsPush(rs, rse);
		rseFree(rse);
	}
}

// Moves the conent of RS to RQ

void rs2rq(RS *rs, RQ *rq) {
	while (!rsIsEmpty(rs)) {
		rqEnQ(rq, rse2rqe(rsTop(rs)));
		rsPop(rs);
	}
}

///////////////////// DB in Memory /////////////////////////////////////////////

// Operations responsible for loading the db into main memory.

MemEntry *memNewEntry(ObjId objId, Type type, String name, OL *ol) {

	MemEntry	*me;

	me = (MemEntry *)malloc(sizeof(MemEntry));
	if (!me) {
		printf("memNewEntry(): out of memory");
		return NULL;
	}

	me->objId	= objId;
	me->type	= type;
	me->name	= name == NULL ? NULL : (char *)strdup(name);
	me->ol		= ol;

	return me;
}

void meDisp(MemEntry *me) {
	printf("MemEntry -> ");
	if (me == NULL)
		printf("%p\n", me);
	else {
		printf("objId = %u  type = %c  name = %s  ", me->objId, me->type, me->name);
		olDisp(me->ol);
	}
}

// MemoryEntry to ESEntry

ESEntry *me2ese(MemEntry *me) {
	return esNewEntry(me->objId, me->type, me->name, me->ol);
}


MemEntry *parseLine(char *line) {

	ObjId	objId;
	Type	type;
	String 	name;
	OL		*ol;
	Object	*o;
	char	*str, *olStr;
	char 	*t;	// can not make it block wise

	str = (char *)malloc(sizeof(char) * 1000);
	olStr = (char *)malloc(sizeof(char) * 1000);

	if (line != NULL && *(line + strlen(line) - 1) == '\n')
		*(line + strlen(line) - 1) = '\0';

	//printf("parseLine(): .%s.\n", line);

	str = (char *)strtok(line, "\t");
	objId = strtoul(str, NULL, 10);

	str = (char *)strtok(NULL, "\t");
	if (strcmp(str, "DB") == 0)
		type = 'A';			// 'A' denotes type DB for the moment
	if (strcmp(str, "CLASS") == 0)
		type = 'C';
	if (strcmp(str, "R") == 0)
		type = 'R';
	if (strcmp(str, "L") == 0)
		type = 'L';
	if (strcmp(str, "S") == 0)
		type = 'S';

	ol = olInit();


	if (type == 'A') {
		name = (char *)strdup(strtok(NULL, "\t"));
		olStr = (char *)strtok(NULL, "\t");

		t = (char *)malloc(sizeof(char) * strlen(olStr));
		t = (char *)strtok(olStr, "|");

		while (t != NULL) {
			o = objNew(t);
			olAdd(ol, o);
			objFree(o);
			t = (char *)strtok(NULL, "|");
		}
	} else if (type == 'C') {

		name = (char *)strdup(strtok(NULL, "\t"));

		str = (char *)strtok(NULL, "\t");

		olStr = (char *)strtok(str, ",");

		while (olStr != NULL) {
			o = objNewO(NULL, strtoul(olStr, NULL, 10));
			olAdd(ol, o);
			objFree(o);
			olStr = (char *)strtok(NULL, ",");
		}

		free(olStr);
	} else {

		name = NULL;

		strtok(NULL, "\t");
		olStr = (char *)strtok(NULL, "\t");

		t = (char *)malloc(sizeof(char) * strlen(olStr));
		t = (char *)strtok(olStr, "|");

		while (t != NULL) {
			o = objNew(t);
			olAdd(ol, o);
			objFree(o);
			t = (char *)strtok(NULL, "|");
		}
  	}

	return memNewEntry(objId, type, name, ol);
}

void dbRead(MemEntry **memDB) {

	FILE	*fp;
	//char 	*fn1 = "/root/ca/MembershipTab";	// need full path here to make spawned child task work.
	//char	*fn2 = "/root/ca/ObjectTab";
	char	*fn1 = "/home/dagong/ca/MembershipTab";
	char	*fn2 = "/home/dagong/ca/ObjectTab";

	char		*line;
	MemEntry	*me;
	ObjId		i = 0;

	for (i = 0; i < MAX_NUM_MEMENTRY; i++) {
		memDB[i] = NULL;
	}

	fp = fopen(fn1, "r");
	if (!fp) {
		printf("dbRead(): can not open file %s", fn1);
		exit(1);
	}

	line = (char *)malloc(sizeof(char) * DB_LINE_MAXLEN);
	i = 0;
	while (fgets(line, DB_LINE_MAXLEN, fp) != NULL) {
		char *s;
		s = (char *)malloc(sizeof(char) * 100);
		s = (char *)strtok_r((char *)strdup(line), "\t", &s);
		i = strtoul(s, NULL, 10);

		memDB[i] = parseLine(line);
	}
	meMax = i;

	fclose(fp);

	fp = fopen(fn2, "r");
	if (!fp) {
		printf("dbRead(): can not open file %s\n", fn2);
		exit(1);
	}

	while (fgets(line, DB_LINE_MAXLEN, fp) != NULL) {
		//printf("dbRead()   : .%s.\n", line);
		char *s;
		s = (char *)malloc(sizeof(char) * 100);
		s = (char *)strtok_r((char *)strdup(line), "\t", &s);
		i = strtoul(s, NULL, 10);

		memDB[i] = parseLine(line);

	}
	meMax = i;

	fclose(fp);
}


void dbDisp(MemEntry **memDB) {
 	ObjId	i;
	printf("\n\ndisplaying content of the db\n\n");
	for (i = 0; i < meMax + 1; i++) {
		if (memDB[i] != NULL) {
			printf("%d ", i);
			meDisp(memDB[i]);
		} else
			printf("%d is an empty slot.\n", i);
	}
}

void disp(ES *es, RS *rs, RQ *q, OL *arg, char *msg) {
	printf("\n------------------------------------------------------------- %s\n\n", msg);
	esDisp(es);
	rsDisp(rs);
	olDisp(arg);
	rqDisp(q);
	printf("-------------------------------------------------------------^ %s\n\n", msg);
}



ESEntry *unnest(MemEntry **memDB, Object *o) {

	ESEntry	*ese;

	switch (o->type) {
	case 'O':	// ObjId
		ese = me2ese(memDB[o->value.objId]);
		break;
	case 'S':
		break;
	case 'D':
		return NULL;
	default:
		printf("unnest(): should not reach this point.\n");
	}
	return ese;
}




/////////////////////////// Miscellaneous ///////////////////////////////////////


void pause(void) {
	int i;
	printf("press enter to continue: ");
	getchar();
}

void pauseMsg(char *msg) {
	printf("\n%s\n", msg);
	pause();
}

