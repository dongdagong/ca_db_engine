Prototyping Communicating Agents Exploiting Distribution and Parallelism
========================================================================

This is the implementation for a research project in distributed database query language. It uses C multi-threading to launch  multiple "communicating agents" talking to each other in serving database queries, and PVM to turn a network of computers into a single concurrent computing environment. 

System Architecture
-------------------

Three PVMs are built. PVM1 consists of two PCs it017599 and it001930; PVM2 consists of two PCS it007501 and it001932; PVM3 is made up of three SPARC workstations it019531, it019532, and it019533, as illustrated here.

.. image:: d01.png

The machines are ethernet connectd and all running Debian. It017599 hosts all the user directories and provides a Network Information Service (NIS). The PVM executables are present on all nodes and need to be up and running before further programs can be launched. 

The diagram below illustrates the logical view of the system. Running on a set of PVM computing nodes (potentially more than three), the Communicating Agents (CA) provides a single uniform query service for the Sample Applications (SA). The SAs have a fixed point of contact in the CAs. This role of contact point is assigned to a particular CA.

.. image:: d02.png 

Communication Mechanism
-----------------------

User processes may utilize one of the following communication mechanisms: 

1) PVM Message Passing (within the same virtual machine.)
 a) Any process can create other new processes in the same virtual machine.
 b) Non-block sending.
 c) Sending steps
   - Initialize buffer
   - Pack data items (Need to flatten structures)
   - Send
 d) Receiving steps
   - Blocked or non-blocking receives.
   - Unpack data items
    
2) Remote Procedure Call (between virtual machines)
  a) Client/server model
  b) Blocked request and waiting server. Asynchronous call can be achieved by initiating the call in a thread. 
  c) Various levels of communication API. Higher level simpler but less control. Lower level more complex but more control.
  
3) POSIX Threading (on the same physical machine)
  a) Threads share the same memory address, communication is the fastest compare to RPC and PVM Message Passing.
  b) Use mutexes to protect share data.
  c) Use condition variables to signal changes to data.


Execution Model
---------------

The Master Agent (ma.c) is started first and it never quits. It waits for a Client's request and tid (task id), then it creates a Master Communicating Agent and send the tid of this MCA back to the Client.

The Client (client.c) has a request in the form of a string, it initializes a Result Queue, then it sends the request and a pointer of the Result Queue to CCM (Client Communication Module). Once CCM is completed with the request and returns, the data in Result Queue is then available to the Client.

The Communicating Agents (ca.c) collaborate to process the Client’s request in a parallel fashion. Processing at different machines should be done in parallel; Partial data should be used by upper level processes as soon as they are available. Two CAs are used in the system and both are spawned using the same C program ca.c. The Master CA is created by Master Agent on the same machine; The Master CA then creates the second CA on another machine. 

.. image:: d03.png 
   :scale: 60 %

Note the use of two communication functions ptRequest() and mpRequest(). 

ptRequest() is used to launch a 2SAM engine on the local machine using POSIX Threading; while mpRequest() is used to receive result, using PVM message passing, from a remote machine where a previously spawned ca.c task is already running. 

Both functions create threads responsible for processing/communication and return immediately. The caller can then choose to either polling if the result is finished, or synchronizing with the created thread using pthread_join or condition variable.

In the previous serial version of this program, the two matchClassNames, join, and projectNames are run one after another. Now matchClassName(StudentC) and matchClassName(PaperOfferingC) can operate concurrently at different machines. But the join and projectName still run in serial. 

It would be desirable if all of them can operate concurrently, exploiting 1) parallel processing at different machines and 2) pipelining. How ever, the algorithm for join and projectNames require that the two matchClassNames be finished first. 

It would also be desirable if the partial result produced by projectName could be sent back to caller (mpRequestThread at Client) as soon as they are available. This is not realized in the program while it could be by putting projectName in a seperate 2SAM thread. 

An observation is that the Result Queue is used for both processing and communication; this prevents the use of pipelining, since partially processed data should not be sent.


Other programs and data files:

- bds.h
	header file for basic data structures, including Objects, Stacks, Queues, etc.
- bds.c
	operations on basic data structures; functions reading disk data files into memory.
- ca.h
	mainly system wide constants.
- cm.h
	defines data structure of the argument passed to mpRequestThread.
- cm.c
	mpRequest() and mpRequestThread().
- Makefile
	compiling and linking commands.
- MembershipTab
	data file describing database and it's classes. (first line modified)
- ObjectTab
	data file describing object instances. (using | as delimiter)
- maTid.txt
	task id of Master Agent, written by Master Agent and read by Client.
- bdstest.c
	tester for bds.c and bds.h


