/* ma.c */
/* Master Agent */


#include <stdio.h>
#include <pvm3.h>
#include "ca.h"

int main() {
	int info;
	int clientTid;
	int caTid;
	int myTid;
	char request[REQUEST_MAX_LEN];

	int node = 0;	// previous node where ca.c is spawned.
	int i;

	FILE *fp;

	printf("\n\n\nma.c main(): ma started >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n");

	myTid = pvm_mytid();
	printf("ma.c myTid = %x \n", myTid);
	fp = fopen(MA_TID_FILE,"w");
	fprintf(fp, "%d\n", myTid);
	fclose(fp);

	pvm_catchout(stdout);

	while(1) {
		// receive client's request and tid

		pvm_recv(-1, -1);
		pvm_upkstr(request);
		pvm_upkint(&clientTid, 1, 1);

		printf("ma.c main(): request = %s   clientTid = %x \n", request, clientTid);

		// determine on which node a MCA should be created
/*
		if (node == pvm[0].numOfNodes - 1)
			node = 0;
		else
			node++;
*/

		// always on it001930
		node = 0;
		//printf("ma is starting MCA at %s \n", pvm[0].node[node]);

		// spawn this MCA
		pvm_spawn("/home/dagong/ca/ca", (char **)0, 1, "it001930", 1, &caTid);

		// return the MCA's tid to client

		printf("ma.c main(): caTid = %x \n\n", caTid);

		pvm_initsend(PvmDataDefault);
		pvm_pkint(&caTid, 1, 1);
		pvm_send(clientTid, 0);
	}
}



