
/*

file: bds.h

Basic Data Structures and operations.

*/

#ifndef Boolean
#define Boolean 	char
#define TRUE		1
#define FALSE 		0
#endif

typedef unsigned char	Type;
typedef char *			String;
typedef void *			Pointer;
typedef unsigned long	ObjId;


// Object Type

typedef struct {

	Type 		type;
	String		name;
	union {
		String 	bvalue;		// B S D I
		Pointer ptr;		// P
		ObjId	objId;		// O
	} value;
} Object;


// Object List Entry Type

typedef struct OLEntryTag{

	Object				*obj;
	struct OLEntryTag	*prev;
	struct OLEntryTag	*next;
} OLEntry;


// Object List Type

typedef struct {

	OLEntry			*first;
	OLEntry			*last;
	OLEntry			*p;	// prev, used for iteration
	OLEntry			*n;	// next, used for iteration


	unsigned long	len;
} OL;


// Object operations

Object	*objNewB(Type, String, String);
Object	*objNewP(String, Pointer);
Object	*objNewO(String, ObjId);
Object	*objNew(char *triple);

Object	*objClone(Object *);
Boolean objComp(Object *, Object *);
void	objFree(Object *o);
void	objDisp(Object *);

// Object List operations

OL		*olInit(void);
Boolean	olIsEmpty(OL *ol);
Boolean	olAdd(OL *ol, Object *obj);
Object	*olGet(OL *ol);		// get the first object
Boolean	olRemove(OL *ol);	// remove the first entry
void	olInitIterator(OL *);
Boolean	olHasNext(OL *);
Object	*olNext(OL *);
void	olRemoveCur(OL *);


OL		*olClone(OL *);
void	olFree(OL *ol);
void	olDisp(OL *ol);


// Environment Stack Entry Type.

typedef struct ESEntryTag {

	ObjId 				objId;

	Type 				type;
	String				name;
	OL					*ol;

	struct ESEntryTag 	*next;
	struct ESEntryTag 	*nextVisibleScope;

} ESEntry;


// Result Stack Entry Type.

typedef struct RSEntryTag {

	ObjId 				objId;

	Type 				type;
	String				name;
	OL					*ol;

	struct RSEntryTag 	*next;

} RSEntry;


// Environment Stack Type.

typedef struct ESTag {

	unsigned long	depth;
	ESEntry 		*top;

} ES;


// Result Stack Type.

typedef struct RSTag {
	unsigned long	depth;
	RSEntry 		*top;
	RSEntry			*pipeFrom;

} RS;


// Environment Stack and Result Stack operations


ESEntry	*esNewEntry(ObjId, Type, String, OL *);
RSEntry	*rsNewEntry(ObjId, Type, String, OL *);

ESEntry	*eseClone(ESEntry *);
RSEntry	*rseClone(RSEntry *);

void	eseFree(ESEntry *);
void	rseFree(RSEntry *);

void	eseDisp(ESEntry *);
void	rseDisp(RSEntry *);


ES		*esInit(void);
RS		*rsInit(void);

Boolean	esIsEmpty(ES *);
Boolean	rsIsEmpty(RS *);

Boolean	esPush(ES *, ESEntry *);	// store pointer
Boolean	rsPush(RS *, RSEntry *);	// store clone

Boolean rsPushTriple(RS *, ObjId, Type, String, char *triple);

ESEntry	*esTop(ES *);
RSEntry	*rsTop(RS *);

Boolean	esPop(ES *);
Boolean	rsPop(RS *);

void	esFree(ES *);	// free entries contained, does not free the stack itself
void	rsFree(RS *);	// free entries contained, does not free the s:tack itself

void	esDisp(ES *);
void	rsDisp(RS *);



// Result Queue

typedef struct RQEntryTag {

	ObjId 				objId;

	Type 				type;
	String				name;
	OL					*ol;

	struct RQEntryTag 	*next;

} RQEntry;

#include <pthread.h>

typedef struct RQTag {

	RQEntry 		*head;
	RQEntry			*tail;
	long			len;	// LONG_MAX +2,147,483,647
	long			maxLen;

	pthread_mutex_t	mutex;	// protect
	pthread_cond_t	avail;	// data available
	pthread_cond_t	ready;	// space ready

	int				finish;	// set to 1 when no more incoming data
}RQ;

// Result Queue operatioins

RQEntry	*rqNewEntry(ObjId, Type, String, OL *);
RQEntry	*rqeClone(RQEntry *);
void	rqeFree(RQEntry *);
void	rqeDisp(RQEntry *);

RQ		*rqInit(unsigned long maxLen);
Boolean	rqIsEmpty(RQ *);
Boolean	rqIsFull(RQ *);
Boolean	rqEnQ(RQ *, RQEntry *);
RQEntry	*rqDeQ(RQ *);

void	rqFree(RQ *);		// free entries contained, does not free the queue itself
void	rqDisp(RQ *);


// Type Conversions

RQEntry	*ese2rqe(ESEntry *);
RSEntry *rqe2rse(RQEntry *rqe);
ESEntry	*rqe2ese(RQEntry *rqe);
ESEntry *rse2ese(RSEntry *);
RQEntry *rse2rqe(RSEntry *rse);

void rq2rs(RQ *rq, RS*rs);
void rs2rq(RS *rs, RQ *rq);

// Everything returned or passed are pointers, if need to transfer value, clone before passing
// the argument or after receiving the return. If clone is needed frequently, can add wrapper
// functions that do return or accept a non-pointer var.



// DB in memory

#define	MAX_NUM_MEMENTRY	10000
#define DB_LINE_MAXLEN		1000

typedef struct {

	ObjId		objId;
	Type		type;
	String		name;
	OL			*ol;	// length contained in OL
} MemEntry;				// one MemEntry corresponds to one line in db file.

ObjId			meMax;


MemEntry	*memNewEntry(ObjId objId, Type type, String name, OL *ol);
void		meDisp(MemEntry *me);
ESEntry		*me2ese(MemEntry *me);
MemEntry	*parseLine(char *line);
void		dbRead(MemEntry **memDB);
void		dbDisp(MemEntry **memDB);
void		disp(ES *, RS *, RQ *, OL *, char *);
ESEntry		*unnest(MemEntry **memDB, Object *o);


// Miscellaneous

void pause(void);
void pauseMsg(char *);
