
/*

file: ca.h

system wide constants

*/


// directories and file names

#define HOME_DIR		"/home/dagong/ca"
#define PC_EXE_DIR		"/home/dagong/ca"
#define SPARC_EXE_DIR	"/home/dagong/ca/sparc"

#define CA				"/home/dagong/ca/ca"
#define CA_OUTPUT_FILE	"caOutput"
#define MA_TID_FILE		"maTid.txt"


// PVM constants

#define NUM_OF_PVMS			1
#define MAX_NUM_OF_NODES	4

typedef struct {
	const char	*node[MAX_NUM_OF_NODES];
	const int	numOfNodes;
} PVM;

PVM pvm[NUM_OF_PVMS] = {
	{{"it001930", "it017599"}, 2},	// PVM00
	//{{"it007501", "it001932"}, 2},						// PVM01
	//{{"it019531", "it019532", "it019533"}, 3}				// PVM02
};


// request and result queue constants

#define REQUEST_MAX_LEN		500
#define MAX_NUM_OF_RQ		100
