CFLAGS = -lpthread -lpvm3
#-L/usr/local/src/pvm3/lib/LINUX
# To make under Redhat, uncomment the line above and combine it with the CFLAGS line, and
# prepend pvm/ to include lines in C files.

all: client ma ca

client: client.o bds.o cm.o bds.h
	cc -o client client.o bds.o cm.o $(CFLAGS)
ma: ma.o bds.o cm.o bds.h
	cc -o ma ma.o bds.o cm.o $(CFLAGS)
ca: ca.o bds.o cm.o bds.h
	cc -o ca ca.o bds.o cm.o $(CFLAGS)

bds.o: bds.c
	cc -c bds.c
cm.o: cm.c
	cc -c cm.c
client.o: client.c
	cc -c client.c
ma.o: ma.c
	cc -c ma.c
ca.o: ca.c
	cc -c ca.c
